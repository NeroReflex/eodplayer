#include "TestOutputDevice.h"

TestOutputDevice::TestOutputDevice(FrameStreamHandler& queueHandler, VideoDataObtainedFn videoDataCallback, VideoStateObtainedFn videoStateCallback) noexcept :
    OutputDevice(queueHandler), m_VideoDataObtainedFn(videoDataCallback), m_VideoStateObtainedFn(videoStateCallback), m_ShouldClose(false)
{
}

TestOutputDevice::~TestOutputDevice() {}

void TestOutputDevice::exec() noexcept
{
    m_ShouldClose                = false;
    std::function<void()> stopFn = [this]() { m_ShouldClose = true; };

    while (!m_ShouldClose) {
        switch (pollDecoderStatus()) {
            case DecoderStatus::WaitingForFile:
                {
                    m_VideoStateObtainedFn("WaitingForFile", stopFn);

                    if (nextStream()) {
                    }
                }
                break;

            case DecoderStatus::Active:
                {
                    m_VideoStateObtainedFn("Active", stopFn);

                    auto possibly_frame = receiveVideoData();

                    if (possibly_frame.has_value()) {
                        const auto& video_data = possibly_frame.value();
                        m_VideoDataObtainedFn(video_data, stopFn);
                    }
                }
                break;

            case DecoderStatus::Paused:
                {
                    m_VideoStateObtainedFn("Paused", stopFn);

                    if (nextStream()) {
                    }
                }
                break;

            case DecoderStatus::Seeking:
                {
                    m_VideoStateObtainedFn("Seeking", stopFn);

                    if (nextStream()) {
                    }
                }
                break;

            case DecoderStatus::Unknown:
                {
                    m_VideoStateObtainedFn("Unknown", stopFn);

                    if (nextStream()) {
                    }
                }
                break;

            case DecoderStatus::WaitingForPlay:
                {
                    m_VideoStateObtainedFn("WaitingForPlay", stopFn);

                    if (nextStream()) {
                    }
                }
                break;

            case DecoderStatus::Stopped:
                {
                    m_VideoStateObtainedFn("Stopped", stopFn);

                    if (nextStream()) {
                    }
                }
                break;
        }
    }
}
