/*
 * My license
 */
#include "FFMPEGDecoder.h"
#include "FrameStreams.h"
#include "TestOutputDevice.h"

#include "Commands/DecoderCommand.h"

#include <cassert>
#include <queue>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#undef CATCH_CONFIG_MAIN
#include <cstdint>

namespace Eodplayer_test
{
std::string nextCommand;

std::allocator<uint8_t> frameMemoryManagerObj;
const static auto       frameMemoryAllocator = [](size_t sz) -> void* {
    void* malloc_result = frameMemoryManagerObj.allocate(sz + sizeof(size_t));

    if (malloc_result) {
        *(reinterpret_cast<size_t*>(malloc_result)) = sz;
    }

    return reinterpret_cast<void*>(reinterpret_cast<char*>(malloc_result) + sizeof(size_t));
};

const static auto frameMemoryDeallocator = [](void* mem) -> void {
    uint8_t* orig_ptr = reinterpret_cast<uint8_t*>(mem) - sizeof(size_t);

    const auto sz = *(reinterpret_cast<uint32_t*>(orig_ptr));

    return frameMemoryManagerObj.deallocate(orig_ptr, sz);
};

static void testingVA(const TestOutputDevice::VideoDataObtainedFn& videoCallback, const TestOutputDevice::VideoStateObtainedFn& stateCallback)
{
    // this is the thread-safe double ended queue used by components that must
    // enqueue and dequeue frames
    std::allocator<FrameStreamHandler::FrameDequeue> allocator;
    FrameStreams                                     frameQueuesHandler(allocator);

    bool        done                = false;
    std::thread outputHandlerThread = std::thread([&]() {
        std::unique_ptr<OutputDevice> debugOutput = std::make_unique<TestOutputDevice>(frameQueuesHandler, videoCallback, stateCallback);
        debugOutput->run();
        debugOutput.reset();
        done = true;
    });

    DequeHM<DecoderCommand*> commandQueue;

    auto decoder = std::make_unique<FFMPEGDecoder>(frameQueuesHandler, frameMemoryAllocator, frameMemoryDeallocator);

    std::this_thread::sleep_for(std::chrono::seconds(2));
    decoder->loadFile("../../samples/gopro_cineform_rgb_16bit_alpha_pre00000000.mov");

    std::this_thread::sleep_for(std::chrono::seconds(2));
    decoder->play();

    std::this_thread::sleep_for(std::chrono::seconds(2));
    while (!done) {
        // I let everything go for a few seconds (to give the tests time to run)
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    }
    decoder->stop();

    outputHandlerThread.join();
    decoder.reset();
}

static void testingCm(const TestOutputDevice::VideoDataObtainedFn& videoCallback, const TestOutputDevice::VideoStateObtainedFn& stateCallback)
{
    // this is the thread-safe double ended queue used by components that must
    // enqueue and dequeue frames
    std::allocator<FrameStreamHandler::FrameDequeue> allocator;
    FrameStreams                                     frameQueuesHandler(allocator);

    bool        done                = false;
    std::thread outputHandlerThread = std::thread([&]() {
        std::unique_ptr<OutputDevice> debugOutput = std::make_unique<TestOutputDevice>(frameQueuesHandler, videoCallback, stateCallback);
        debugOutput->run();
        debugOutput.reset();
        done = true;
    });

    DequeHM<DecoderCommand*> commandQueue;

    auto decoder = std::make_unique<FFMPEGDecoder>(frameQueuesHandler, frameMemoryAllocator, frameMemoryDeallocator);

    nextCommand = "Unknown";
    std::this_thread::sleep_for(std::chrono::seconds(2));

    while (!done) {
        if (nextCommand == "WaitingForFile") {
            decoder->loadFile("../../samples/gopro_cineform_rgb_16bit_alpha_pre00000000.mov");
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            nextCommand = "loadFile";
        } else if (nextCommand == "WaitingForPlay") {
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            decoder->play();
            nextCommand = "Active1";
        } else if (nextCommand == "seek") {
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            decoder->seek(1);
            nextCommand = "Seeking";
        } else if (nextCommand == "pause") {
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            decoder->pause();
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            nextCommand = "Paused";
        } else if (nextCommand == "resume") {
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            decoder->resume();
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            nextCommand = "Active2";
        } else if (nextCommand == "stop") {
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            decoder->stop();
            // std::this_thread::sleep_for(std::chrono::seconds(2));
            nextCommand = "Fine";
        }
    }

    outputHandlerThread.join();
    decoder.reset();
}

TEST_CASE("test pixel and audio", "[test decoder]")
{
    int                                  nFrame = 0;
    int                                  nAudio = 0;
    std::vector<std::array<uint16_t, 4>> pixelsRgbAlpha;
    std::vector<std::array<uint16_t, 4>> pixelsRgb;
    std::vector<std::array<uint16_t, 4>> pixelsAlpha;
    std::vector<std::array<uint16_t, 4>> pixelsNero;

    TestOutputDevice::VideoDataObtainedFn videoCallback = [&](const VideoData& vd, const std::function<void()>& stopFn) {
        int rgbAlpha  = 0;
        int rgb       = 0;
        int alpha     = 0;
        int pixelNeri = 0;

        if (vd.hasFrame()) {
            const auto& frame = vd.getFrame();
            nFrame++;

            switch (frame.getPixelFormat()) {
                case Frame::PixelFormat::RGBA64:
                    {
                        // this contains 4-color tuples expressed as a 16-bit number. A pixel is 4 colors, so pixels_colors [0], pixels_colors [1],
                        // pixels_colors [2], pixels_colors [3] are the first pixel, pixels_colors [4], pixels_colors [5], pixels_colors [6], pixels_colors
                        // [7] are the second pixel, and so on ... the pixels are frame.getHeight () * frame.getWidth () and are organized by horizontal lines
                        const auto pixels_colors = reinterpret_cast<const uint16_t*>(frame.getRawBuffer());

                        // image [300] [400] [0] means the red component (0) of the 400 x 300 pixel
                        std::vector<std::vector<std::array<uint16_t, 4>>> immagine;

                        // Iterate over the lines of the video (that would be the height of the 1080 video)
                        for (size_t h = 0; h < frame.getHeight(); ++h) {
                            std::vector<std::array<uint16_t, 4>> pixels_linea_orizzontale;

                            // Iterate over the number of pixels that make up an image (in pixels = 1920x1080 -> 2073600)
                            for (size_t w = 0; w < frame.getWidth(); ++w) {
                                const auto i = (h * frame.getWidth()) + w;
                                pixels_linea_orizzontale.push_back(
                                    {pixels_colors[(i * 4) + 0], pixels_colors[(i * 4) + 1], pixels_colors[(i * 4) + 2], pixels_colors[(i * 4) + 3]}
                                );
                            }

                            immagine.emplace_back(std::move(pixels_linea_orizzontale));
                        }

                        // I only run the checks at the sixtieth frame
                        if (nFrame == 60) {
                            // I check that the 99x796 pixel has the following rgb and alpha values
                            REQUIRE(immagine[796][99][0] == 59918);
                            REQUIRE(immagine[796][99][1] == 60126);
                            REQUIRE(immagine[796][99][2] == 60174);
                            REQUIRE(immagine[796][99][3] == 65535);

                            // I check that the 399x5 pixel is black
                            REQUIRE(immagine[399][5][0] == 4097);
                            REQUIRE(immagine[399][5][1] == 4097);
                            REQUIRE(immagine[399][5][2] == 4097);
                            REQUIRE(immagine[399][5][3] == 0);

                            for (size_t h = 0; h < frame.getHeight(); ++h) {
                                for (size_t w = 0; w < frame.getWidth(); ++w) {

                                    if ((immagine[h][w][0] != 4097 || immagine[h][w][1] != 4097 || immagine[h][w][2] != 4097) && immagine[h][w][3] != 0) {
                                        // I count the pixels that have the rgb channel and the alpha
                                        rgbAlpha++;
                                    } else if ((immagine[h][w][0] != 4097 || immagine[h][w][1] != 4097 || immagine[h][w][2] != 4097) && immagine[h][w][3] == 0) {
                                        // I count the pixels that have the rgb channel and not the alpha
                                        rgb++;
                                    } else if ((immagine[h][w][0] == 4097 && immagine[h][w][1] == 4097 && immagine[h][w][2] == 4097) && immagine[h][w][3] != 0) {
                                        // I count the pixels that do not have the rgb channel but the alpha
                                        alpha++;
                                    } else if (immagine[h][w][0] == 4097 && immagine[h][w][1] == 4097 && immagine[h][w][2] == 4097 && immagine[h][w][3] == 0) {
                                        // I count black pixels (Values -> r = 4097; g = 4097; b = 4097; alpha = 0)
                                        pixelNeri++;
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        if (vd.hasAudio()) {
            nAudio++;
            const auto& audio = vd.getAudioBuffer();

            if (nAudio == 1) {
                // Check the number of audio channels
                REQUIRE(audio.getChannelsCount() == 1);
                // Check audio sample number 1
                REQUIRE(audio.getSamplesCount() == 925);
            } else if (nAudio == 120) {
                // Check the number of audio channels
                REQUIRE(audio.getChannelsCount() == 1);
                // Check audio sample number 120
                REQUIRE(audio.getSamplesCount() == 412);
            }
        }

        // I stop playing after the 60th and 120th sample audio frames
        if (nFrame == 60 && nAudio == 120) {
            // Pixels rgb + alpha
            REQUIRE(rgbAlpha == 297304);
            // Pixels only rgb
            REQUIRE(rgb == 44341);
            // Pixels only alpha
            REQUIRE(alpha == 0);
            // Black pixels
            REQUIRE(pixelNeri == 1731955);
            // Total pixels
            REQUIRE(rgbAlpha + rgb + alpha + pixelNeri == 2073600);
            // Total audio samples
            REQUIRE(nAudio == 120);

            stopFn(); // Calling this function terminates the execution of the output device
        }
    };

    TestOutputDevice::VideoStateObtainedFn stateCallback = [&](const std::string& state, const std::function<void()>& stopFn) {};

    testingVA(videoCallback, stateCallback);
}

TEST_CASE("test decoder command", "[test command]")
{

    TestOutputDevice::VideoDataObtainedFn videoCallback = [&](const VideoData& vd, const std::function<void()>& stopFn) {};

    TestOutputDevice::VideoStateObtainedFn stateCallback = [&](const std::string& state, const std::function<void()>& stopFn) {
        // Check that the commands sent correspond to the commands executed
        if (nextCommand == "Unknown") {
            if (state == "Unknown") {
                REQUIRE(state == "Unknown");
            } else {
                REQUIRE(state == "WaitingForFile");
                nextCommand = "WaitingForFile";
            }
        } else if (nextCommand == "loadFile") {
            if (state == "WaitingForFile") {
                REQUIRE(state == "WaitingForFile");
            } else if (state == "Stopped") {
                REQUIRE(state == "Stopped");
            } else {
                REQUIRE(state == "WaitingForPlay");
                nextCommand = "WaitingForPlay";
            }
        } else if (nextCommand == "Active1") {
            if (state == "WaitingForPlay") {
                REQUIRE(state == "WaitingForPlay");
            } else {
                REQUIRE(state == "Active");
                nextCommand = "seek";
            }
        } else if (nextCommand == "Seeking") {
            if (state == "Seeking") {
                REQUIRE(state == "Seeking");
            } else {
                REQUIRE(state == "Active");
                nextCommand = "pause";
            }
        } else if (nextCommand == "Paused") {
            if (state == "Active") {
                REQUIRE(state == "Active");
            } else if (state == "Seeking") {
                REQUIRE(state == "Seeking");
            } else {
                REQUIRE(state == "Paused");
                nextCommand = "resume";
            }
        } else if (nextCommand == "Active2") {
            if (state == "Paused") {
                REQUIRE(state == "Paused");
            } else {
                REQUIRE(state == "Active");
                nextCommand = "stop";
            }
        } else if (nextCommand == "Fine") {
            if (state == "Active") {
                REQUIRE(state == "Active");
            } else {
                REQUIRE(state == "Stopped");
                nextCommand = "Stopped";
            }
        }

        // Stop execution after the stop command
        if (nextCommand == "Stopped") {
            stopFn();
        }
    };

    testingCm(videoCallback, stateCallback);
}

} // namespace Eodplayer_test
