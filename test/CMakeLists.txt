#--Set Testing--------------------------------------------------------------------------
add_executable(eodplayer_test)

target_sources(eodplayer_test
        PUBLIC  ${PROJECT_SOURCE_DIR}/test/TestOutputDevice.h
        PRIVATE ${PROJECT_SOURCE_DIR}/test/TestOutputDevice.cc
        PRIVATE ${PROJECT_SOURCE_DIR}/test/Eodplayer_test.cc
        PRIVATE ${PROJECT_SOURCE_DIR}/third_party/catch2/catch.hpp
)

target_include_directories(eodplayer_test
        PUBLIC ${PROJECT_SOURCE_DIR}/include
)

target_include_directories(eodplayer_test SYSTEM
        PUBLIC ${PROJECT_SOURCE_DIR}/third_party/catch2
)

target_link_libraries(eodplayer_test
        PUBLIC EODPlayer
)

# Used to launch sanitizers
target_compile_options(eodplayer_test
        PRIVATE ${COMPILER_FLAGS_TSAN} # Variable created
        PRIVATE ${COMPILER_FLAGS_ASAN}
        PRIVATE ${COMPILER_FLAGS_LSAN}
        PRIVATE ${COMPILER_FLAGS_MSAN}
        PRIVATE ${COMPILER_FLAGS_USAN}
)

# Used to launch sanitizers
target_link_options(eodplayer_test
        PRIVATE ${LINKER_FLAGS_TSAN} # Variable created
        PRIVATE ${LINKER_FLAGS_ASAN}
        PRIVATE ${LINKER_FLAGS_LSAN}
        PRIVATE ${LINKER_FLAGS_MSAN}
        PRIVATE ${LINKER_FLAGS_USAN}
)

#--Add Testing--------------------------------------------------------------------------
enable_testing()

add_test(NAME Catch_Test
         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
         COMMAND ${CMAKE_BINARY_DIR}/bin/eodplayer_test
)

add_test(NAME Valgrind_test
         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
         COMMAND valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes
         --track-origins=yes
         ${CMAKE_BINARY_DIR}/bin/eodplayer_test
)
# --show-error-list=yes
# --leak-check=full
# --show-leak-kinds=all