#pragma once

#include "OutputDevice.h"

#include "DequeHM.h"

class TestOutputDevice : public OutputDevice
{
  public:
    typedef std::function<void(const VideoData&, const std::function<void()>&)> VideoDataObtainedFn;

    typedef std::function<void(const std::string&, const std::function<void()>&)> VideoStateObtainedFn;

    TestOutputDevice(FrameStreamHandler& queueHandler, VideoDataObtainedFn videoDataCallback, VideoStateObtainedFn videoStateCallback) noexcept;

    TestOutputDevice(const TestOutputDevice&) = delete;

    TestOutputDevice& operator=(const TestOutputDevice&) = delete;

    TestOutputDevice& operator=(TestOutputDevice&&) = delete;

    TestOutputDevice(TestOutputDevice&&) = delete;

    ~TestOutputDevice() override;

  protected:
    void exec() noexcept final;

  private:
    VideoDataObtainedFn m_VideoDataObtainedFn;

    VideoStateObtainedFn m_VideoStateObtainedFn;

    bool m_ShouldClose;
};
