*Read this in english: [English](README.md)*

# **_EODPLAYER_**

In questo progetto si è andati a realizzare un player video che permette di utilizzare e leggere il canale Alpha oltre al normale canale RGB. 
Solitamente, il canale alpha viene frequentemente utilizzato sia nel mondo cinematografico che sportivo. 
Il player realizzato permette di riprodurre i comuni formati video sia che contengano il canale alpha o meno e come tutti i classici player possiede i comandi di  load, play, pause, resume, seek e stop.
I comandi elencati qui sopra vengono ricevuti dal player tramite una connessione socket, in modo da poterlo controllare anche da distanza.

## Installazione

Per poter utilizzare il player sul proprio pc **_Linux_**, **_Windows_** o **_Mac_** che sia, sarà necessario andare ad installare le seguiti librerie:
* libsdl2-dev
* libopengl-dev
* ninja-build
* libglew-dev
* ffmpeg
	* libavutil-dev
    * libavformat-dev
    * libavdevice-dev
    * libswscale-dev
    * libavcodec-dev
    * libswresample-dev
    * libavfilter-dev
* libpthread-stubs0-dev

Oltre ad esse sarà anche necessario andare ad installare i seguenti programmi per compilare il progetto ed installarlo:
* [Clang](https://clang.llvm.org/) o [gdb](https://www.sourceware.org/gdb/)
* [CMake](https://cmake.org/)
* make

#### Compilazione in modalità debug
```bash
cd eodplayer
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

#### Compilazione in modalità release
```bash
cd eodplayer
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

In fine, nel caso in cui si voglia generare anche la documentazione del codice bastera installare:
* [Doxygen](https://doxygen.nl/)
* [graphviz](https://graphviz.org/)

#### Generazione della documentazione
```bash
cd eodplayer/build
make doc
```

#### Esecuzione dei test
```bash
cd eodplayer/build
make test
```

## Utilizzo
Per utilizzare il player basterà avviarlo ed eseguire i comandi (**_load_**, **_play_**, **_pause_**, **_resume_**, **_seek_** e **_stop_**) da console così da poter interagire con esso.

#### Load file
```bash
cd eodplayer/build/bin
./load ${percorso_file_video}
```

#### Play
```bash
cd eodplayer/build/bin
./play
```

#### Pause
```bash
cd eodplayer/build/bin
./pause
```

#### Resume
```bash
cd eodplayer/build/bin
./resume
```

#### Seek
```bash
cd eodplayer/build/bin
./seek ${tempo_in_secondi}
```

#### Stop
```bash
cd eodplayer/build/bin
./stop
```

## Autori
* *Denis Benato*
* *Lorenzo Genghini*

## Stato del progetto
Lavori in corso

## Licenza
Open source