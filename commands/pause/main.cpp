#include <iostream>
#include <cstdlib>
#include <cinttypes>
#include <array>
#include <string>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT     8080

int main(int argc, char** argv) {
    //std::array<uint8_t, 1024> buffer;
    //buffer[0] = 0x00;

    int sockfd;
    struct sockaddr_in     servaddr;

    std::string commandText;
    commandText = "pause";

    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        std::cerr << "socket creation failed" << std::endl;

        return EXIT_FAILURE;
    }

    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = INADDR_ANY;

    //int n;

    socklen_t len;

    sendto(sockfd, commandText.c_str(), commandText.size(),
           MSG_CONFIRM, (const struct sockaddr *) &servaddr,
           sizeof(servaddr));

    std::cout << "Pause command sent." << std::endl;

    close(sockfd);

    return EXIT_SUCCESS;
}