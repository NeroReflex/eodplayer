*Read this in italian: [Italian](README.IT.md)*

# **_EODPLAYER_**

In this project, a video player was created that allows you to use and read the Alpha channel in addition to the normal RGB channel.
Typically, the alpha channel is frequently used in both the film and sports world.
The player created allows you to play common video formats whether they contain the alpha channel or not and, like all classic players, it has the load, play, pause, resume, seek and stop commands.
The commands listed above are received by the player via a socket connection, so that it can also be controlled remotely.

## Installation

In order to use the player on your pc **_Linux_**, **_Windows_** or **_Mac_** whatever, you will need to install the following libraries:
* libsdl2-dev
* libopengl-dev
* ninja-build
* libglew-dev
* ffmpeg
	* libavutil-dev
    * libavformat-dev
    * libavdevice-dev
    * libswscale-dev
    * libavcodec-dev
    * libswresample-dev
    * libavfilter-dev
* libpthread-stubs0-dev

In addition to them it will also be necessary to install the following programs to compile the project and install it:
* [Clang](https://clang.llvm.org/) or [gdb](https://www.sourceware.org/gdb/)
* [CMake](https://cmake.org/)
* make

#### Compiling in debug mode
```bash
cd eodplayer
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

#### Compilation in release mode
```bash
cd eodplayer
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

Finally, if you also want to generate the code documentation, just install:
* [Doxygen](https://doxygen.nl/)
* [graphviz](https://graphviz.org/)

#### Documentation generation
```bash
cd eodplayer/build
make doc
```

#### Execution of tests
```bash
cd eodplayer/build
make test
```

## Usage
To use the player just start it and execute the commands (**_load_**, **_play_**, **_pause_**, **_resume_**, **_seek_** and **_stop_**) from the console as well to be able to interact with it.

#### Load file
```bash
cd eodplayer/build/bin
./load ${video_file_path}
```

#### Play
```bash
cd eodplayer/build/bin
./play
```

#### Pause
```bash
cd eodplayer/build/bin
./pause
```

#### Resume
```bash
cd eodplayer/build/bin
./resume
```

#### Seek
```bash
cd eodplayer/build/bin
./seek ${time_in_seconds}
```

#### Stop
```bash
cd eodplayer/build/bin
./stop
```

## Authors
* *Denis Benato*
* *Lorenzo Genghini*

## Status of the project
Works in progress

## License
Open source