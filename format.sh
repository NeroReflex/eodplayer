#!/bin/sh
clang-format -i source/*.c
clang-format -i source/*.cpp
clang-format -i source/*.cxx

clang-format -i source/Commands/*.c
clang-format -i source/Commands/*.cpp
clang-format -i source/Commands/*.cxx

clang-format -i include/*.h
clang-format -i include/*.hpp
clang-format -i include/*.hxx

clang-format -i include/Commands/*.h
clang-format -i include/Commands/*.hpp
clang-format -i include/Commands/*.hxx

clang-format -i test/*.c
clang-format -i test/*.cpp
clang-format -i test/*.cxx
clang-format -i test/*.h
clang-format -i test/*.hpp
clang-format -i test/*.hxx