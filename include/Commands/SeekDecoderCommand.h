#pragma once

#include "Commands/DecoderCommand.h"

class SeekDecoderCommand : public DecoderCommand
{
  public:
    SeekDecoderCommand(double time) noexcept;

    ~SeekDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;

  private:
    double timeToJump;
};
