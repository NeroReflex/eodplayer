#pragma once

#include "Commands/DecoderCommand.h"

class PauseDecoderCommand : public DecoderCommand
{
  public:
    PauseDecoderCommand() noexcept;

    ~PauseDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;
};
