#pragma once

#include "Commands/DecoderCommand.h"

class StopDecoderCommand : public DecoderCommand
{
  public:
    StopDecoderCommand() noexcept;

    ~StopDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;
};
