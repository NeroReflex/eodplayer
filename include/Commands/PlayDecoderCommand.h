#pragma once

#include "Commands/DecoderCommand.h"

class PlayDecoderCommand : public DecoderCommand
{
  public:
    PlayDecoderCommand() noexcept;

    ~PlayDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;
};
