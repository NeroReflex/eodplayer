#pragma once

#include "Commands/DecoderCommand.h"

class LoadFileDecoderCommand : public DecoderCommand
{
  public:
    LoadFileDecoderCommand(Decoder::FileNameType fileName) noexcept;

    ~LoadFileDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;

  private:
    Decoder::FileNameType m_FileName;
};
