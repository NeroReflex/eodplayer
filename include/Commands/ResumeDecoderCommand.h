#pragma once

#include "Commands/DecoderCommand.h"

class ResumeDecoderCommand : public DecoderCommand
{
  public:
    ResumeDecoderCommand() noexcept;

    ~ResumeDecoderCommand() override;

    void execute(Decoder* target) const noexcept final;
};
