#pragma once

#include "OutputDevice.h"

#include "DequeHM.h"

class FakeOutputDevice : public OutputDevice
{

  public:
    FakeOutputDevice(FrameStreamHandler& queueHandler) noexcept;

    ~FakeOutputDevice() override;

    void exec() noexcept final;
};
