#pragma once

#include "DecoderStatus.h"
#include "FrameStreamHandler.h"
#include "OutputDevice.h"

/**
 * @brief The decoder application logic
 *
 * This class represents the public interface of a video decoder.
 *
 * Any video decoder is capable of executing all required operation for the
 * player, loading a file or stream and play it as well as stopping it; seek
 * operations can be unsupported this is to support stream.
 *
 * Every method MUST be non-blocking and everything that needs to happen in
 * "backgroud" as the decode operation MUST live in it's own thread (that can be
 * manipulated by aforementioned non-blocking methods).
 *
 * Playback is done emitting frames (objects of type Frame) with actual data
 * inside them, this means that allocator and deallocator callback must also be
 * provided.
 *
 * A video decoder is not meant to be an object shared between threads: is has
 * to be implemented as its public methods are called by the creating thread and
 * it MUST be used that way.
 */
class Decoder
{

  public:
    typedef std::string FileNameType;

    /**
     * @brief Construct a new Decoder object in a Stopped status
     *
     * Every decoder MUST call this constructor keeping in mind that lifetime of
     * objects used inside the two provided functions must be carefully thought
     * as there are no guarantees as to when the deallocate function will be
     * called.
     *
     * @param outputQueue the output queue frames will be sent to
     * @param allocate the memory allocator function that will be called to fill
     * a new frame with pixel data
     * @param deallocate the memory deallocator function that will be called
     * when a decoded frame is not needed anymore
     */
    Decoder(FrameStreamHandler& queueHandler, const Frame::MallocFunctionType& allocate, const Frame::DeallocFunctionType& deallocate) noexcept;

    /**
     * @brief Destroy the Decoder object
     *
     * Free every used resource and stops the actual decoding if the object is
     * deleted while playing a video file.
     */
    virtual ~Decoder();

    Decoder(const Decoder&) = delete;

    Decoder(Decoder&&) = delete;

    Decoder& operator=(const Decoder&) = delete;

    Decoder& operator=(Decoder&&) = delete;

    /**
     * @brief Loads a file into the decoder
     *
     * If the decoder is actually producing frames from a file this command
     * also stops the playback and prepare everything necessary to execute a
     * play command on that other file.
     *
     * @param filename the identifier of the file
     */
    virtual void loadFile(const FileNameType& filename) noexcept = 0;

    /**
     * @brief Launch a thread that decodes frames from the loaded video file
     *
     * Non-blocking method that is responsible for starting
     * an async operation that will take care of decoding the previously loaded
     * file.
     *
     * It is the developer responsibility to call emitFrame as soon as possible
     * when a new frame is generated in the decoding thread.
     */
    virtual void play() noexcept = 0;

    /**
     * @brief Stop the thread that decodes frames
     *
     * Non-blocking method that is responsible for signaling the active decoding
     * thread to pause.
     *
     * If no thread is active calling this function is a no-operation.
     */
    virtual void stop() noexcept = 0;

    /**
     * @brief Pause the video decoding
     *
     * Non-blocking method that is responsible for signaling the active decoding
     * thread to pause and to keep ready to be resumed.
     *
     * If no thread is active this function is a no-operation.
     *
     * If the active thread already is paused this function is a no-operation.
     */
    virtual void pause() noexcept = 0;

    /**
     * @brief request a seek on the video stream
     *
     * Non-blocking method that is responsible for signaling the active decoding
     * thread to seek to a specific point in time,
     *
     * The seek will target the backward frame closer to the given time point.
     *
     * If no thread is active this function is a no-operation.
     *
     * If the active thread is paused this function will work and the resume will
     * resume from the seeked point.
     *
     * If the data source cannot be seeked this operation is a no-op.
     *
     * @param time the time of the seeking point expressed in seconds
     */
    virtual void seek(double time) noexcept = 0;

    /**
     * @brief Resume the video decoding
     *
     * Non-blocking method that is responsible for signaling the active decoding
     * thread to resume decoding,
     *
     * If no thread is active this function is a no-operation.
     *
     * If the active thread already is decoding this function is a no-operation.
     */
    virtual void resume() noexcept = 0;

    /**
     * @brief Get the video decoding status
     *
     * Non-blocking method that returns to the caller the decoder status.
     *
     * @return the decoder status
     */
    DecoderStatus getStatus() const noexcept;

  protected:
    /**
     * @brief change the decoder status and propagate the change to the stream
     *
     * Changes the current status of the decoder while also propagating the new
     * status as the status of the active frame stream.
     *
     * @param status the new status of the decoder
     */
    void setStatus(const DecoderStatus& status) noexcept;

    /**
     * @brief register the current decoder
     *
     * This function perform the decoder registration into the
     * FrameStreamHandler.
     *
     * @return true IIF the decoder is allowed to continue
     */
    bool startDecoding() noexcept;

    /**
     * @brief unregister the current decoder
     *
     * This function perform the decoder unregistration into the
     * FrameStreamHandler.
     */
    void endDecoding() noexcept;

    /**
     * @brief create an empty stream, controlling the status of the old stream
     * and the new one.
     *
     * This function can only be called after a successful call to
     * startDecoding() and before the corresponding call to endDecoding().
     *
     * This function creates an empty stream with the provided DecoderStatus
     * and, if provided, sets the provided status to the current one,
     *
     * @param newStreamStatus the new status of the new stream
     * @param oldStreamStatus the new status of the old stream
     */
    void createEmptyStream(const DecoderStatus& newStreamStatus, const std::optional<DecoderStatus>& oldStreamStatus = std::optional<DecoderStatus>()) noexcept;

    /**
     * @brief Emit a frame decoded by the playback thread
     *
     * This method is supposed to be called by the frame decoder function when a
     * frame has been decoded and needs to be sent to the output device.
     *
     * This is a blocking function that can make the user wait if there is
     * actually not enough room to store the given frame or the exec method is
     * accessing the video data collection in a way that allows the queue to remain
     * in a valid state.
     *
     * @param pf the frame pixel format
     * @param width the frame width (in pixels)
     * @param height the frame height (in pixels)
     * @param frameFillerFn the function that is responsible to fill the frame
     * with provided information
     */
    void emitFrame(
        Frame::PixelFormat                              pf,
        uint32_t                                        width,
        uint32_t                                        height,
        int64_t                                         timebase_num,
        int64_t                                         timebase_den,
        int64_t                                         timestamp,
        const BufferBackedData::DataFillerFunctionType& frameFillerFn
    ) const noexcept;

    /**
     * @brief Emit an audio buffer decoded by the playback thread
     *
     * This method is supposed to be called by the frame decoder function when a
     * frame has been decoded and needs to be sent to the output device.
     *
     * This is a blocking function that can make the user wait if there is
     * actually not enough room to store the given frame or the exec method is
     * accessing the video data collection in a way that allows the queue to remain
     * in a valid state.
     *
     * @param sampleFormat the audio sample format
     * @param channelsNo the number of audio channels
     * @param samplesNo the number of samples in each channel
     * @param frameFillerFn the function that is responsible to fill the audio buffer
     * with provided information
     */
    void emitAudioBuffer(
        AudioBuffer::SampleFormat                       sampleFormat,
        uint16_t                                        channelsNo,
        uint32_t                                        samplesNo,
        int64_t                                         timebase_num,
        int64_t                                         timebase_den,
        int64_t                                         timestamp,
        const BufferBackedData::DataFillerFunctionType& audioFillerFn
    ) const noexcept;

    /**
     * @brief Count the frames in flight
     *
     * This method can be used before decoding a frame to avoid overwhelming
     * the output device with frames that will fill up the entire RAM.
     *
     * This is a non-blocking function that is designed to give an estimation
     * that is useful for the above purpose.
     *
     * @return an estimation of frame in flight
     */
    size_t countFramesInFlight() const noexcept;

  private:
    FrameStreamHandler& m_FramesQueuesHandler;

    FrameStreamHandler::FrameWriter* mFrameWriter;

    Frame::MallocFunctionType m_AllocatorFn;

    Frame::DeallocFunctionType m_DeallocatorFn;

    std::atomic<DecoderStatus> m_Status;
};
