#pragma once

#include "FrameStreams.h"

#include "Commands/DecoderCommand.h"
#include "Commands/LoadFileDecoderCommand.h"
#include "Commands/PauseDecoderCommand.h"
#include "Commands/PlayDecoderCommand.h"
#include "Commands/ResumeDecoderCommand.h"
#include "Commands/SeekDecoderCommand.h"
#include "Commands/StopDecoderCommand.h"

/**
 * Class that allows the emission of DecoderCommand(s),
 * so that the video playback can be controlled in a implementation-dependant way.
 */
class CommandReceiver
{

  public:
    /**
     * @brief Constructor to be called by every subclass
     *
     * @param commandsQueue the queue that will receive generated commands
     */
    explicit CommandReceiver(DequeHM<DecoderCommand*>& commandsQueue) noexcept;

    CommandReceiver(const CommandReceiver&) = delete;

    CommandReceiver(CommandReceiver&&) = delete;

    CommandReceiver& operator=(const CommandReceiver&) = delete;

    CommandReceiver& operator=(CommandReceiver&&) = delete;

    virtual ~CommandReceiver();

    /**
     * @brief starts the thread responsible for accepting commands
     *
     * This function creates a thread that will listen for incoming commands and
     * enqueues them so that the decoder can execute them.
     *
     * If a thread is already running it will be closed and re-opened, in which
     * case the previously returned function is to be considered invalid.
     *
     * @return the function that will shutdown the current thread or an empty optional in case of error(s)
     */
    virtual std::optional<std::function<void(void)>> run() noexcept = 0;

  protected:
    /**
     * @brief the method that the thread MUST call to enqueue a command
     *
     * Writes the command to the queue bound at object creation time.
     *
     * @param command the command to be enqueued
     */
    void enqueueCommand(DecoderCommand* command) noexcept;

  private:
    DequeHM<DecoderCommand*>& m_CommandsQueue;
};