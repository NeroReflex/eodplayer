#pragma once

#include "config.h"

// STL streams
#include <fstream>
#include <iostream>
#include <sstream>

// STL data types
#include <cinttypes>
#include <cstdbool>
#include <cstddef>
#include <functional>
#include <optional>
#include <string>

// STL containers
#include <array>
#include <forward_list>
#include <initializer_list>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <vector>

// STL memory
#include <memory>

// STL algorithms
#include <algorithm>
#include <limits>
#include <utility>

// STL thread
#include <future>
#include <mutex>
#include <thread>
//#include <semaphore> // Old GCC doesn't have this

#if defined(SUPPORT_OPENGL)

// OpenGL
#include <GL/glew.h>

#endif
