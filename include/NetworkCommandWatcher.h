#pragma once

#include "CommandReceiver.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * Class that allows the emission of DecoderCommand(s) from network input,
 * so that the video playback can be controlled remotely.
 */
class NetworkCommandWatcher : public CommandReceiver
{

  public:
    /**
     * @brief create the NetworkCommandWatcher object
     *
     * Creates the NetworkCommandWatcher object in a reset state that allows the
     * user to call run();
     *
     * @param commandsQueue
     */
    explicit NetworkCommandWatcher(DequeHM<DecoderCommand*>& commandsQueue) noexcept;

    NetworkCommandWatcher(const NetworkCommandWatcher&) = delete;

    NetworkCommandWatcher(NetworkCommandWatcher&&) = delete;

    NetworkCommandWatcher& operator=(const NetworkCommandWatcher&) = delete;

    NetworkCommandWatcher& operator=(NetworkCommandWatcher&&) = delete;

    /**
     * Closes the thread if it's still running and free all memory acquired by
     * the current object.
     */
    ~NetworkCommandWatcher() override;

    /**
     * @brief starts the thread responsible for accepting commands via network
     *
     * This function creates a thread that will listen for incoming commands and
     * enqueues them so that the decoder can execute them.
     *
     * If a thread is already running it will be closed and re-opened, in which
     * case the previously returned function is to be considered invalid.
     *
     * @return the function that will shutdown the current thread or an empty optional in case of error(s)
     */
    std::optional<std::function<void(void)>> run() noexcept override;

  private:
    void reset() noexcept;

    void watch() noexcept;

    std::unique_ptr<std::thread> m_NetworkHandlerThread;

    std::atomic_bool m_ShouldStop;

    int m_Socket;

    struct sockaddr_in servaddr;
};