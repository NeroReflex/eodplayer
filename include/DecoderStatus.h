#pragma once

#include "EODPlayer.hpp"

enum class DecoderStatus {
    Unknown,
    WaitingForFile,
    WaitingForPlay,
    Seeking,
    Active,
    Paused,
    Stopped,
};