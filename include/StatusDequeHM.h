#pragma once

#include "DequeHM.h"

/**
 * This class represents a double ended queue that two process can use to share
 * messages in a thread-safe manner while also propagating information from the
 * producer thread to consumer thread(s).
 *
 * @tparam S the status
 * @tparam T the type of messages to be shared
 * @tparam Allocator the allocator to be used to dynamically allocate messages
 */
template <typename S, typename T, typename Allocator = std::allocator<T>> class StatusDequeHM : public DequeHM<T, Allocator>
{
  public:
    StatusDequeHM(const S& status = S()) noexcept;
    ~StatusDequeHM();

    void writeStatus(const S& status) noexcept;
    S    readStatus() noexcept;

  private:
    std::atomic<S> m_Status;
};

template <typename S, typename T, typename Allocator> StatusDequeHM<S, T, Allocator>::StatusDequeHM(const S& status) noexcept : m_Status(status) {}

template <typename S, typename T, typename Allocator> StatusDequeHM<S, T, Allocator>::~StatusDequeHM() {}

template <typename S, typename T, typename Allocator> void StatusDequeHM<S, T, Allocator>::writeStatus(const S& status) noexcept
{
    m_Status = status;
}

template <typename S, typename T, typename Allocator> S StatusDequeHM<S, T, Allocator>::readStatus() noexcept
{
    return m_Status;
}