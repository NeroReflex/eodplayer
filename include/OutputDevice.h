#pragma once

#include "FrameStreamHandler.h"

#include "DequeHM.h"

/**
 * @brief Represents an output device for image frames.
 *
 * An output device is responsible to emit frames on a user-accessible device,
 * and to allow the user to terminate the output stream.
 */
class OutputDevice
{

  public:
    typedef FrameStreamHandler::FrameDequeue MultithreadedQueueType;

    /**
     * @brief Construct a new OutputDevice object
     *
     * Every subclass MUST call this constructor.
     *
     * @param queueHandler the queue handler that is used by the registered
     * decoder to store frames
     */
    OutputDevice(FrameStreamHandler& queueHandler) noexcept;

    /**
     * @brief Destroy the OutputDevice
     *
     * Every class implementing this interface MUST use the desctructor
     * to deallocate every resource: this includes destructing every staged
     * frame (this implies calling the destructor function on all frames).
     */
    virtual ~OutputDevice();

    OutputDevice(const OutputDevice&) = delete;

    OutputDevice(OutputDevice&&) = delete;

    OutputDevice& operator=(const OutputDevice&) = delete;

    OutputDevice& operator=(OutputDevice&&) = delete;

    void run() noexcept;

  protected:
    /**
     * @brief Execute the main cycle
     *
     * This method MUST implement a loop that can be user-interrupted
     * or error-interrupted and while active it shows frames to the user
     * when the right time comes.
     *
     * In general there shouldn't be any interaction with the user outside from
     * this method.
     *
     * This method is supposed to be a blocking call that only returns when the
     * loop is interrupted,
     *
     * If there are no frames to show (as the playback is not running or the
     * video is seeking) it is supposed to show the last valid frame.
     */
    virtual void exec() noexcept = 0;

    /**
     * @brief Dequeue the next decoded VideoData object by the decoder.
     *
     * This method is supposed to be called very frequently by the exec method.
     *
     * This is a blocking function that can make the user wait if that is the
     * only way that allows the queue to remain in a valid state.
     *
     * @return Either the next decoded frame or an empty optional
     */
    std::optional<VideoData> receiveVideoData() const noexcept;

    /**
     * @brief Fetch the decoder status so that the player can take the best
     * action
     *
     * The decoder has an internal status and will act accordingly, for example
     * when paused it will not produce frames, but frames produced before the
     * pause will still be reproduced by a player that won't check the status,
     * this is bad as the pause command could be arrived from the user that in
     * the worst case scenario will see frames reproduced for more than one
     * second before the pause.
     *
     * Also, in case of a pause stopping reproduction immediately will alleviate
     * the pressure on decoder at resume, as it will have time to decode new
     * frames while the playback will continue on frames decoded by the decoder
     * before the pause; obviously this is a best-case and desired scenario,
     * both decoder and OutputDevice MUST be designed to not rely on this to
     * happen as that might introduce lags.
     *
     * As for a stopped stream it might means a few things, for example the
     * decoder is not yet started or the decoder has finished decoding a video
     * file... the suggested operation for this status is to just call popFrame,
     * if no frame is returned than either the decoder has not yet started or it
     * has finished and the video has been reproduced completely and a change
     * stream should be attempted, otherwise there are decoder frames that MUST
     * be reproduced as the decoder has decoded the whole file but final frames
     * are still on-the-fly ready to be shown to the user.
     *
     * This function MUST be used to prevent this type of situations.
     *
     * @return the decoder status
     */
    DecoderStatus pollDecoderStatus() const noexcept;

    /**
     * @brief switch to the following Frame stream
     *
     * This method is supposed to be called infrequently by the exec method.
     *
     * This is a blocking function that can make the user wait if that is the
     * only way that allows the queue handler to remain in a valid state.
     *
     * @return a boolean value indicating operation success
     */
    bool nextStream() noexcept;

  private:
    FrameStreamHandler& m_QueuesHeandler;

    FrameStreamHandler::FrameReader* mStreamReader;
};
