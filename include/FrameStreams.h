#pragma once

#include "Frame.h"
#include "FrameStreamHandler.h"
#include "StatusDequeHM.h"

/**
 * @brief An implementation of FrameStreamHandler
 *
 * This object uses double-ended queues of Frame objects to input and output
 * frames, therefore an allocator for that data structure MUST be provided
 * (defaults to the C++ default memory allocator).
 *
 * @tparam Alloc the allocator for data structure holding frames
 */
template <typename Alloc = std::allocator<FrameStreamHandler::FrameDequeue>> class FrameStreams : public FrameStreamHandler
{

  public:
    class FrameReader : public FrameStreamHandler::FrameReader
    {
      public:
        FrameReader(FrameStreams* streams, FrameStreams::FrameDequeue* frameStream) noexcept;

        bool useNextStream() noexcept final;

      private:
        FrameStreams* mStreams;
    };

    class FrameWriter : public FrameStreamHandler::FrameWriter
    {
      public:
        FrameWriter(FrameStreams* streams, FrameStreams::FrameDequeue* frameStream) noexcept;

        void createNextStream(const DecoderStatus& status) noexcept final;

      private:
        FrameStreams* mStreams;
    };

    friend class FrameReader;
    friend class FrameWriter;

    /**
     * @brief Construct a FrameStreams object that will use the provided memory
     * allocator
     *
     * @param alloc the memory allocator to be used for memory allocations
     */
    FrameStreams(Alloc& alloc) noexcept;

    ~FrameStreams() override = default;

    FrameStreams::FrameReader* registerOutputDevice(OutputDevice* outDev) noexcept final;

    void unregisterOutputDevice(OutputDevice* outDev) noexcept final;

    FrameStreams::FrameWriter* registerDecoder(Decoder* decoder) noexcept final;

    void unregisterDecoder(Decoder* decoder) noexcept final;

  private:
    Alloc mAllocator;

    uintptr_t mRegisteredDecoder;

    uintptr_t mRegisteredOutputDevice;

    std::mutex mLock;

    std::list<std::shared_ptr<FrameStreams::FrameDequeue>> mUnactiveFrameStreams;

    std::shared_ptr<FrameStreams::FrameDequeue> mActiveDecoderFrameStream;

    std::shared_ptr<FrameStreams::FrameDequeue> mActiveOutputDeviceFrameStream;

    FrameStreams::FrameReader* mFrameReader;

    FrameStreams::FrameWriter* mFrameWriter;

    // std::forward_list<size_t, std::weak_ptr<FrameDequeue>> mFrameDequeues;

    // std::atomic_bool
};

template <typename Alloc> FrameStreams<Alloc>::FrameReader::FrameReader(FrameStreams* streams, FrameStreams::FrameDequeue* frameStream) noexcept :
    FrameStreamHandler::FrameReader(frameStream), mStreams(streams)
{
}

template <typename Alloc> bool FrameStreams<Alloc>::FrameReader::useNextStream() noexcept
{
    std::lock_guard<std::mutex> lck(mStreams->mLock);

    auto oldActiveStream = mActiveQueue;

    if (mStreams->mUnactiveFrameStreams.empty()) {
        mStreams->mActiveOutputDeviceFrameStream = mStreams->mActiveDecoderFrameStream;
    } else {
        mStreams->mActiveOutputDeviceFrameStream = mStreams->mUnactiveFrameStreams.front();
        mStreams->mUnactiveFrameStreams.pop_front();
    }

    mActiveQueue = mStreams->mActiveOutputDeviceFrameStream.get();

    return oldActiveStream != mActiveQueue;
}

template <typename Alloc> void FrameStreams<Alloc>::FrameWriter::createNextStream(const DecoderStatus& status) noexcept
{
    std::lock_guard<std::mutex> lck(mStreams->mLock);

    mStreams->mUnactiveFrameStreams.push_back(mStreams->mActiveDecoderFrameStream);

    mStreams->mActiveDecoderFrameStream = std::allocate_shared<FrameStreams::FrameDequeue, Alloc>(mStreams->mAllocator, status);

    mActiveQueue = mStreams->mActiveDecoderFrameStream.get();
}

template <typename Alloc> FrameStreams<Alloc>::FrameWriter::FrameWriter(FrameStreams* streams, FrameStreams::FrameDequeue* frameStream) noexcept :
    FrameStreamHandler::FrameWriter(frameStream), mStreams(streams)
{
}

template <typename Alloc> FrameStreams<Alloc>::FrameStreams(Alloc& alloc) noexcept :
    mAllocator(alloc), mRegisteredDecoder(0), mRegisteredOutputDevice(0),
    mActiveDecoderFrameStream(std::allocate_shared<FrameStreams::FrameDequeue, Alloc>(mAllocator, DecoderStatus::Unknown)),
    mActiveOutputDeviceFrameStream(mActiveDecoderFrameStream)
{
}

template <typename Alloc> typename FrameStreams<Alloc>::FrameReader* FrameStreams<Alloc>::registerOutputDevice(OutputDevice* outDev) noexcept
{
    std::lock_guard<std::mutex> lck(mLock);

    // if an invalid OutputDevice was passed or there is already one registered
    // return an empty optional meaning registration was unsuccessful
    if ((!outDev) || (mRegisteredOutputDevice != 0)) {
        return nullptr;
    }

    mRegisteredOutputDevice = reinterpret_cast<uintptr_t>(outDev);
    return mFrameReader     = new FrameStreams::FrameReader(this, mActiveDecoderFrameStream.get());
}

template <typename Alloc> void FrameStreams<Alloc>::unregisterOutputDevice(OutputDevice* outDev) noexcept
{
    std::lock_guard<std::mutex> lck(mLock);

    if ((!outDev) || (mRegisteredOutputDevice == 0) || (mRegisteredOutputDevice != reinterpret_cast<uintptr_t>(outDev))) {
        return;
    }

    mRegisteredOutputDevice = 0;
    delete mFrameReader;
    mFrameReader = nullptr;
}

template <typename Alloc> typename FrameStreams<Alloc>::FrameWriter* FrameStreams<Alloc>::registerDecoder(Decoder* decoder) noexcept
{
    std::lock_guard<std::mutex> lck(mLock);

    // if an invalid OutputDevice was passed or there is already one registered
    // return an empty optional meaning registration was unsuccessful
    if ((!decoder) || (mRegisteredDecoder != 0)) {
        return nullptr;
    }

    mRegisteredDecoder  = reinterpret_cast<uintptr_t>(decoder);
    return mFrameWriter = new FrameStreams::FrameWriter(this, mActiveDecoderFrameStream.get());
}

template <typename Alloc> void FrameStreams<Alloc>::unregisterDecoder(Decoder* decoder) noexcept
{
    std::lock_guard<std::mutex> lck(mLock);

    if ((!decoder) || (mRegisteredDecoder == 0) || (mRegisteredDecoder != reinterpret_cast<uintptr_t>(decoder))) {
        return;
    }

    mRegisteredDecoder = 0;
    delete mFrameWriter;
    mFrameWriter = nullptr;
}