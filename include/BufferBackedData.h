#pragma once

#include "EODPlayer.hpp"

/**
 * @brief This class represents a raw buffer to be used to store video objects
 * such as frames or audio buffers, subtitles and so on.
 *
 * As for an object of this class to actually holds data a memory allocation is
 * required.
 *
 * An object of this class is filled with data via a function call that has
 * parameters not stored inside the object, therefore an object of this class
 * cannot be copied using copy-constructors.
 *
 * Also, executing a copy would be a slow operation that could copy a lot of
 * data and potentially do a system call to obtain new memory and such
 * slowness may compromise video player efficiency and introduce random
 * delays.
 *
 * An object of this class can however be moved and that move operation will be
 * really fast.
 *
 * This class is not intended to be created, instead its purpose is to be subclassed
 * to have an object that describes a well-defined portion of a video such as a frame
 * or an audio fragment.
 */
class BufferBackedData
{

  public:
    typedef std::function<void*(size_t)> MallocFunctionType;
    typedef std::function<void(void*)>   DeallocFunctionType;
    typedef std::function<void(void*)>   DataFillerFunctionType;

    BufferBackedData(const BufferBackedData&) = delete;

    BufferBackedData& operator=(const BufferBackedData&) = delete;

    BufferBackedData(BufferBackedData&& src) noexcept;

    BufferBackedData& operator=(BufferBackedData&& src) noexcept;

    virtual ~BufferBackedData();

    /**
     * @brief Return a value indicating the presence of an allocated buffer.
     *
     * @return true IIF the internal buffer has been allocated and not moved out
     * @return false IIF the internal buffer has not been allocated or was moved
     */
    [[nodiscard]] bool isHoldingData() const noexcept;

    /**
     * @brief Get a pointer to the raw buffer holding data
     *
     * The raw buffer will be fully populated with data when isHoldingData()
     * returns TRUE.
     *
     * Return the pointer to the raw buffer as a void pointer. The real data
     * layout in memory depends on the final object type (audio, video etc..) and
     * internal format (RGBA64, YUV etc...),
     *
     * Using the returned value without accounting for isHoldingData() value is
     * an error and could lead to SEGV.
     *
     * @return the pointer to allocated data or nullptr if the frame is not
     * holding any data
     */
    [[nodiscard]] void* getRawBuffer() const noexcept;

    /**
     * @brief Get the size of allocated buffer or zero.
     *
     * @return the size of the allocated buffer or zero if the buffer
     * was moved out of this object or never allocated
     */
    [[nodiscard]] size_t getRawBufferSize() const noexcept;

  protected:
    /**
     * @brief Default constructor, can only be called by subclasses so that it's impossible
     * to create an object of this type.
     *
     * Constructs an object that doesn't hold any data and has no internal buffer filled.
     */
    BufferBackedData() noexcept;

    /**
     * @brief Fill frame data with actual pixels.
     *
     * This function is meant to be called by a method in the subclass.
     *
     * @param bytes the minimum number of bytes to be allocated for the buffer to
     * hold the entire data that will be filler by the filler function
     * @param mallocFn this is the function that will be called synchronously
     * (inside the method call) that is responsible for allocating the specified
     * amount of bytes on the heap
     * @param deallocFn this is the function that will be called by the
     * object destructor so it's important to think about lifetime of captures
     * @param fillerFn this is the function that will be called synchronously
     * (inside the method call) that is responsible for filling the raw buffer
     */
    void storeData(size_t bytes, const MallocFunctionType& mallocFn, const DeallocFunctionType& deallocFn, const DataFillerFunctionType& fillerFn) noexcept;

  private:
    class BufferDeleter
    {
      private:
        DeallocFunctionType m_Deleter;

      public:
        explicit BufferDeleter(DeallocFunctionType delFn) noexcept;

        template <class T> void operator()(T* p) noexcept { m_Deleter(reinterpret_cast<void*>(p)); }
    };

    std::unique_ptr<uint8_t[], BufferDeleter> m_RawBuffer;

    size_t m_BufferSize;
};