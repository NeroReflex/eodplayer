#pragma once

#include "OutputDevice.h"

#include "DequeHM.h"

#include <SDL2/SDL.h>

#include <chrono>

void fill_audio(void* udata, Uint8* stream, int len) noexcept;

/**
 * @brief An OutputDevice implemented with OpenGL for simplicity and ease of
 * compilation.
 *
 * An instance of this device MUST live in own thread, as the OpenGL context,
 * unlike vulkan is per-thread.
 *
 * This class also relies on SDL2, however the constructor does NOT call SDLInit
 * as this might prevent other instances of this video output to be created,
 * therefore SDL initialization and deinitialization MUST be taken care by the
 * application itself.
 */
class OpenGLOutputDevice : public OutputDevice
{

    friend void fill_audio(void* udata, Uint8* stream, int len) noexcept;

  public:
    OpenGLOutputDevice(
        FrameStreamHandler& queueHandler, int window_width, int window_height

    ) noexcept;

    OpenGLOutputDevice(const OpenGLOutputDevice&) = delete;

    OpenGLOutputDevice& operator=(const OpenGLOutputDevice&) = delete;

    OpenGLOutputDevice& operator=(OpenGLOutputDevice&&) = delete;

    OpenGLOutputDevice(OpenGLOutputDevice&&) = delete;

    ~OpenGLOutputDevice() override;

  protected:
    void exec() noexcept final;

  private:
    int compileProgram(Frame::PixelFormat pf, const std::string& vertexShader, const std::string& fragmentShader) noexcept;

    std::unordered_map<Frame::PixelFormat, GLuint> m_RenderingPrograms;

    Frame::PixelFormat m_ActiveProgram;

    struct FrameOpenGLStructure {
        GLuint frameVAO;

        GLuint imageTexture;

        double timestamp;

        Frame::PixelFormat pf;
    };

    struct AudioStructure {
        std::vector<uint8_t> soundBuffer;

        double timestamp;
    };

    void RenderBlackFrame() noexcept;

    void RenderFrame(std::list<FrameOpenGLStructure>::iterator it) noexcept;

    void PrepareFrameForRendering(const VideoData& frame) noexcept;

    SDL_AudioSpec m_WantedAudioSpec;

    SDL_AudioSpec m_ObtainedAudioSpec;

    int m_WindowWidth, m_WindowHeight;

    SDL_Window* m_Window;

    GLfloat ClearColor[4] = {0.0f, 0.0f, 0.0f, 0.0f};

    SDL_GLContext m_Context;

    bool m_ContextInitialized;

    bool m_Running;

    std::list<FrameOpenGLStructure> m_OpenGLRenderingData;

    std::list<FrameOpenGLStructure> m_RecycledOpenGLRenderingData;

    std::mutex m_AudioMutex;

    std::list<AudioStructure> m_AudioData;

    struct FrameRenderedInfo {
        std::chrono::time_point<std::chrono::high_resolution_clock> rendertime;

        double timestamp;
    };

    std::optional<FrameRenderedInfo> m_LastFrameRendered;

    GLuint m_EmptyFrameVAO;

    GLuint m_EmptyImageTexture;

    static constexpr size_t MaxReadyFrames = 16; // taking in account FullHD (about 16MB for a frame) and the video
                                                 // memory on a raspberry 512MB
};
