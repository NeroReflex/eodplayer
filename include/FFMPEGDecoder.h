#pragma once

#include "Decoder.h"

/**
 * @brief The implementation of a decoder that uses FFMPEG.
 *
 * Uses libavcoded, which is part of ffmpeg to decode video files.
 */
class FFMPEGDecoder final : public Decoder
{

  public:
    FFMPEGDecoder(FrameStreamHandler& queueHandler, const Frame::MallocFunctionType& allocate, const Frame::DeallocFunctionType& deallocate) noexcept;

    ~FFMPEGDecoder() final;

    void loadFile(const FileNameType& filename) noexcept final;

    void play() noexcept final;

    void stop() noexcept final;

    void pause() noexcept final;

    void resume() noexcept final;

    void seek(double time) noexcept final;

  private:
    void decode() noexcept;

    std::thread m_FFMPEGThread;

    Decoder::FileNameType m_LoadedFilename;

    std::atomic_bool m_ShouldClose;

    std::atomic_bool m_LoadFileRequested;

    std::atomic_bool m_PlayRequested;

    std::atomic_bool m_PauseRequested;

    std::atomic_bool m_ResumeRequested;

    std::atomic_bool m_JumpRequested;

    std::atomic<double> m_JumpTo;

    std::atomic_bool m_StopRequested;

    static constexpr size_t MaxAudioBufferSizePerChannel = 8192;

    static constexpr size_t MaxVideoDataInFlight = 56;
};
