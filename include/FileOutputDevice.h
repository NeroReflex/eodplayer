#pragma once

#include "OutputDevice.h"

#include "DequeHM.h"

class FileOutputDevice : public OutputDevice
{

  public:
    FileOutputDevice(FrameStreamHandler& queueHandler) noexcept;

    ~FileOutputDevice() override;

    void exec() noexcept final;

  private:
    void writeFrame(const std::string& colorFilename, const std::string& alphaFilename, const Frame& frame) const noexcept;
};
