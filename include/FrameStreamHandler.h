#pragma once

#include "DecoderStatus.h"
#include "StatusDequeHM.h"
#include "VideoData.h"

class OutputDevice;
class Decoder;

/**
 * Handles multiple streams of frames from a Decoder to one OutputDevice.
 *
 * Lifetime: an instance of this class (or of a subclass) MUST remain valid as
 * long as there is at least either one OutputDevice or one Decoder registered
 * (this is because as those components are allowed to use FrameReader or
 * FrameWriter while they are in a registered state and those object are
 * pointing to an instance of this object).
 */
class FrameStreamHandler
{
  public:
    typedef StatusDequeHM<DecoderStatus, VideoData> FrameDequeue;

    class FrameReader
    {
      public:
        FrameReader(FrameStreamHandler::FrameDequeue* fdq) noexcept;

        virtual ~FrameReader();

        std::optional<VideoData> popVideoData() noexcept;

        DecoderStatus fetchStatus() noexcept;

        /**
         * @brief change the stream to the following one if any
         *
         * This methods change the active stream of Frame objects to the next
         * one or to the current one if it's the latest one available.
         *
         * @return true iif the stream has been changed
         */
        virtual bool useNextStream() noexcept = 0;

      protected:
        FrameStreamHandler::FrameDequeue* mActiveQueue;
    };

    class FrameWriter
    {
      public:
        FrameWriter(FrameStreamHandler::FrameDequeue* fdq) noexcept;

        virtual ~FrameWriter();

        void pushVideoData(VideoData&& vd) noexcept;

        size_t countFrames() const noexcept;

        void changeStatus(const DecoderStatus& status) noexcept;

        virtual void createNextStream(const DecoderStatus& status) noexcept = 0;

      protected:
        FrameStreamHandler::FrameDequeue* mActiveQueue;
    };

    FrameStreamHandler() noexcept;

    virtual ~FrameStreamHandler();

    /**
     * @brief register an OutputDevice object
     *
     * An output device in order to decode streams MUST be registered within
     * this frame stream.
     *
     * @param outDev the output device that will be allowed to read frames
     * @return the only object that the output device MUST use when a frame is
     * to be read
     */
    virtual FrameStreamHandler::FrameReader* registerOutputDevice(OutputDevice* outDev) noexcept = 0;

    /**
     * @brief unregister an OutputDevice object
     *
     * Allows another OutputDevice to be registered in place of the unregistered
     * one.
     *
     * This call does nothing if called with a null pointer or with an
     * output device that is not actually registered.
     *
     * It is OutputDevice's own responsibility to not use its previously
     * obtained FrameReader as that will result in undefined behaviour.
     *
     * @param outDev the output device that will not be allowed to read frames
     * anymore
     */
    virtual void unregisterOutputDevice(OutputDevice* outDev) noexcept = 0;

    /**
     * @brief register a Decoder object
     *
     * An output device in order to decode streams MUST be registered within
     * this frame stream.
     *
     * @param decoder the decoder that will be allowed to write frames
     * @return the only object that the Decoder MUST use when a frame is to be
     * read
     */
    virtual FrameStreamHandler::FrameWriter* registerDecoder(Decoder* decoder) noexcept = 0;

    /**
     * @brief unregister a Decoder object
     *
     * Allows another Decoder to be registered in place of the unregistered one.
     *
     * This call does nothing if called with a null pointer or with an
     * output device that is not actually registered.
     *
     * It is Decoder's own responsibility to not use its previously obtained
     * FrameWriter as that will result in undefined behaviour.
     *
     * @param decoder the decoder that will not be allowed to write frames
     * anymore
     */
    virtual void unregisterDecoder(Decoder* decoder) noexcept = 0;
};
