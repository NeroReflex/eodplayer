#pragma once

#include "AudioBuffer.h"
#include "Frame.h"

class VideoData
{

  public:
    VideoData(int64_t timebase_num, int64_t timebase_den, int64_t timestamp) noexcept;

    VideoData(const VideoData&) = delete;

    VideoData& operator=(const VideoData&) = delete;

    VideoData(VideoData&& vd) noexcept;

    VideoData& operator=(VideoData&& vd) noexcept;

    int64_t getTimeBaseNum() const noexcept;

    int64_t getTimeBaseDen() const noexcept;

    int64_t getTimeTicks() const noexcept;

    void emplaceFrame(Frame&& frame) noexcept;

    void emplaceAudioBuffer(AudioBuffer&& audio) noexcept;

    bool hasFrame() const noexcept;

    const Frame& getFrame() const noexcept;

    bool hasAudio() const noexcept;

    const AudioBuffer& getAudioBuffer() const noexcept;

  private:
    int64_t m_TimeBaseNum;

    int64_t m_TimeBaseDen;

    int64_t m_TimeTicks;

    std::optional<Frame> m_Frame;

    std::optional<AudioBuffer> m_AudioBuffer;
};