#pragma once

#include "BufferBackedData.h"

class AudioBuffer : public BufferBackedData
{

  public:
    enum class SampleFormat { SINT32, DOUBLE, FLOAT };

    /**
     * @brief Get the size of a sample (in bytes)
     *
     * Helper static function that given a sample format return the dimension in
     * bytes of a sample of the given format.
     *
     * @param sf the sample format
     * @return size_t number of bytes required to store a sample in the specified
     * format
     */
    static size_t getSampleSizeInBytes(SampleFormat sf) noexcept;

    /**
     * @brief Construct an AudioBuffer object with the specified sample format,
     * number of channels and audio samples
     *
     * Do not allocates data for the internal buffer to handle the specified
     * sample format: that operation is done at filling-time using the
     * storeAudioData method.
     *
     * @param sf the sample format for each sample in the audio data
     * @param channelsCount the number of audio channels
     * @param samplesCount the number of audio samples per channel
     */
    AudioBuffer(SampleFormat sf, uint16_t channelsCount, uint32_t samplesCount) noexcept;

    AudioBuffer(const AudioBuffer&) = delete;

    AudioBuffer(AudioBuffer&& src) noexcept;

    AudioBuffer& operator=(const AudioBuffer&) = delete;

    AudioBuffer& operator=(AudioBuffer&& src) noexcept;

    ~AudioBuffer();

    /**
     * @brief Get the sample format for audio data
     *
     * @return the sample format
     */
    SampleFormat getSampleFormat() const noexcept;

    /**
     * @brief Get the number of audio channels
     *
     * @return the number of channels
     */
    uint16_t getChannelsCount() const noexcept;

    /**
     * @brief Get the number of samples for each audio channel
     *
     * @return the number of samples
     */
    uint32_t getSamplesCount() const noexcept;

    /**
     * @brief Fill audio buffer data with actual audio samples
     *
     * @param mallocFn this is the function that will be called synchronously
     * (inside the method call) that is responsible for allocating the specified
     * amount of bytes on the heap
     * @param deallocFn this is the function that will be called by the
     * object destructor so it's important to think about lifetime of captures
     * @param fillerFn this is the function that will be called synchronously
     * (inside the method call) that is responsible for filling the raw buffer
     */
    void storeAudioData(const MallocFunctionType& mallocFn, const DeallocFunctionType& deallocFn, const std::function<void(void*)>& fillerFn) noexcept;

  private:
    SampleFormat m_SampleFormat;

    uint16_t m_ChannelsCount;

    uint32_t m_SamplesCount;
};