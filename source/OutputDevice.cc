#include "OutputDevice.h"

OutputDevice::OutputDevice(FrameStreamHandler& queue) noexcept : m_QueuesHeandler(queue) {}

OutputDevice::~OutputDevice() {}

std::optional<VideoData> OutputDevice::receiveVideoData() const noexcept
{
    return mStreamReader->popVideoData();
}

bool OutputDevice::nextStream() noexcept
{
    return mStreamReader->useNextStream();
}

DecoderStatus OutputDevice::pollDecoderStatus() const noexcept
{
    return mStreamReader->fetchStatus();
}

void OutputDevice::run() noexcept
{
    // acquire the privilege to read frames from video streams
    mStreamReader = m_QueuesHeandler.registerOutputDevice(this);

    if (!mStreamReader) {
        std::cerr << "couldn't register this output device with to the frame "
                     "queues handler"
                  << std::endl;

        return;
    }

    exec();

    // allow other instances of OutputDevice to read from streams
    m_QueuesHeandler.unregisterOutputDevice(this);
}