#include "Decoder.h"

Decoder::Decoder(FrameStreamHandler& queueHandler, const Frame::MallocFunctionType& allocate, const Frame::DeallocFunctionType& deallocate) noexcept :
    m_FramesQueuesHandler(queueHandler), m_AllocatorFn(allocate), m_DeallocatorFn(deallocate), m_Status(DecoderStatus::WaitingForFile)
{
}

Decoder::~Decoder() {}

void Decoder::emitAudioBuffer(
    AudioBuffer::SampleFormat                       sf,
    uint16_t                                        channelsNo,
    uint32_t                                        samplesNo,
    int64_t                                         timebase_num,
    int64_t                                         timebase_den,
    int64_t                                         timestamp,
    const BufferBackedData::DataFillerFunctionType& audioFillerFn
) const noexcept
{
    AudioBuffer ab(sf, channelsNo, samplesNo);
    ab.storeAudioData(m_AllocatorFn, m_DeallocatorFn, audioFillerFn);

    VideoData vd(timebase_num, timebase_den, timestamp);
    vd.emplaceAudioBuffer(std::move(ab));

    // move the video data (fast operation) to the output device as here it's
    // not needed anymore
    mFrameWriter->pushVideoData(std::move(vd));
}

void Decoder::emitFrame(
    Frame::PixelFormat                   pf,
    uint32_t                             width,
    uint32_t                             height,
    int64_t                              timebase_num,
    int64_t                              timebase_den,
    int64_t                              timestamp,
    const Frame::DataFillerFunctionType& frameFillerFn
) const noexcept
{
    // create the frame and fill it with actual data
    Frame frame(pf, width, height);
    frame.storeFrameData(m_AllocatorFn, m_DeallocatorFn, frameFillerFn);

    VideoData vd(timebase_num, timebase_den, timestamp);
    vd.emplaceFrame(std::move(frame));

    // move the video data (fast operation) to the output device as here it's
    // not needed anymore
    mFrameWriter->pushVideoData(std::move(vd));
}

bool Decoder::startDecoding() noexcept
{
    mFrameWriter = m_FramesQueuesHandler.registerDecoder(this);

    const bool allowedToStart = mFrameWriter != nullptr;

    mFrameWriter->changeStatus(getStatus());

    if (!allowedToStart) {
        std::cerr << "Error registering this Decoder to the frame queues handler" << std::endl;
    }

    return allowedToStart;
}

void Decoder::endDecoding() noexcept
{
    m_FramesQueuesHandler.unregisterDecoder(this);
}

void Decoder::createEmptyStream(const DecoderStatus& newStreamStatus, const std::optional<DecoderStatus>& oldStreamStatus) noexcept
{
    if (oldStreamStatus.has_value()) {
        mFrameWriter->changeStatus(oldStreamStatus.value());
    }

    // let the next decoder have a new empty frame queue to work with
    mFrameWriter->createNextStream(newStreamStatus);
}

DecoderStatus Decoder::getStatus() const noexcept
{
    return m_Status;
}

void Decoder::setStatus(const DecoderStatus& status) noexcept
{
    m_Status = status;

    mFrameWriter->changeStatus(status);
}

size_t Decoder::countFramesInFlight() const noexcept
{
    return mFrameWriter->countFrames();
}