#include "BufferBackedData.h"

#include "Frame.h"

static const BufferBackedData::DeallocFunctionType defaultDeallocFn = [](void*) {};

BufferBackedData::BufferBackedData() noexcept : m_RawBuffer(nullptr, BufferDeleter(defaultDeallocFn)), m_BufferSize(0) {}

BufferBackedData::BufferBackedData(BufferBackedData&& src) noexcept : m_RawBuffer(std::move(src.m_RawBuffer)), m_BufferSize(src.m_BufferSize)
{
    src.m_RawBuffer  = std::unique_ptr<uint8_t[], BufferDeleter>(nullptr, BufferDeleter(defaultDeallocFn));
    src.m_BufferSize = 0;
}

BufferBackedData& BufferBackedData::operator=(BufferBackedData&& src) noexcept
{
    if (&src != this) {
        m_RawBuffer      = std::move(src.m_RawBuffer);
        m_BufferSize     = src.m_BufferSize;
        src.m_RawBuffer  = std::unique_ptr<uint8_t[], BufferDeleter>(nullptr, BufferDeleter(defaultDeallocFn));
        src.m_BufferSize = 0;
    }

    return *this;
}

BufferBackedData::~BufferBackedData() {}

bool BufferBackedData::isHoldingData() const noexcept
{
    return m_RawBuffer != nullptr;
}

void* BufferBackedData::getRawBuffer() const noexcept
{
    return m_RawBuffer.get();
}

BufferBackedData::BufferDeleter::BufferDeleter(BufferBackedData::DeallocFunctionType delFn) noexcept : m_Deleter(std::move(delFn)) {}

void BufferBackedData::storeData(
    size_t bytes, const MallocFunctionType& mallocFn, const DeallocFunctionType& deallocFn, const std::function<void(void*)>& fillerFn
) noexcept
{
    // allocate bytes on the heap to store the data
    m_RawBuffer = std::unique_ptr<uint8_t[], BufferDeleter>(reinterpret_cast<uint8_t*>(mallocFn(bytes)), BufferDeleter(deallocFn));

    // allows the caller to fill the allocated buffer with pixel data in the
    // specified format
    fillerFn(reinterpret_cast<void*>(m_RawBuffer.get()));

    m_BufferSize = bytes;
}

size_t BufferBackedData::getRawBufferSize() const noexcept
{
    return m_BufferSize;
}
