#include "FrameStreamHandler.h"

FrameStreamHandler::FrameStreamHandler() noexcept {}

FrameStreamHandler::~FrameStreamHandler() {}

FrameStreamHandler::FrameReader::FrameReader(FrameStreamHandler::FrameDequeue* fdq) noexcept : mActiveQueue(fdq) {}

FrameStreamHandler::FrameReader::~FrameReader() {}

FrameStreamHandler::FrameWriter::FrameWriter(FrameStreamHandler::FrameDequeue* fdq) noexcept : mActiveQueue(fdq) {}

FrameStreamHandler::FrameWriter::~FrameWriter() {}

DecoderStatus FrameStreamHandler::FrameReader::fetchStatus() noexcept
{
    return mActiveQueue->readStatus();
}

std::optional<VideoData> FrameStreamHandler::FrameReader::popVideoData() noexcept
{
    return mActiveQueue->pop_front();
}

void FrameStreamHandler::FrameWriter::pushVideoData(VideoData&& vd) noexcept
{
    return mActiveQueue->push_back(std::move(vd));
}

void FrameStreamHandler::FrameWriter::changeStatus(const DecoderStatus& status) noexcept
{
    return mActiveQueue->writeStatus(status);
}

size_t FrameStreamHandler::FrameWriter::countFrames() const noexcept
{
    return mActiveQueue->size();
}