#include "Frame.h"

static const Frame::DeallocFunctionType defaultDeallocFn = [](void*) {};

size_t Frame::getPixelSizeInBytes(PixelFormat pf) noexcept
{
    switch (pf) {
        case Frame::PixelFormat::RGBA64:
            return sizeof(uint16_t) * 4;
    }

    // this MUST NOT happen
    return 0;
}

Frame::Frame(PixelFormat pf, uint32_t width, uint32_t height) noexcept : BufferBackedData(), m_PixelFormat(pf), m_Width(width), m_Height(height) {}

Frame::Frame(Frame&& src) noexcept : BufferBackedData(std::move(src)), m_PixelFormat(src.m_PixelFormat), m_Width(src.m_Width), m_Height(src.m_Height) {}

Frame& Frame::operator=(Frame&& src) noexcept
{
    if (&src != this) {
        BufferBackedData::operator=(std::move(src));
        m_PixelFormat = src.m_PixelFormat;
        m_Width       = src.m_Width;
        m_Height      = src.m_Height;
    }

    return *this;
}

Frame::~Frame() {}

Frame::PixelFormat Frame::getPixelFormat() const noexcept
{
    return m_PixelFormat;
}

uint32_t Frame::getWidth() const noexcept
{
    return m_Width;
}

uint32_t Frame::getHeight() const noexcept
{
    return m_Height;
}

void Frame::storeFrameData(const MallocFunctionType& mallocFn, const DeallocFunctionType& deallocFn, const std::function<void(void*)>& fillerFn) noexcept
{
    // decide the pixel size (in bytes) to be used to the call to storeData()
    const size_t pixelSize     = getPixelSizeInBytes(getPixelFormat());
    const size_t requiredBytes = pixelSize * getWidth() * getHeight();

    this->storeData(requiredBytes, mallocFn, deallocFn, fillerFn);
}
