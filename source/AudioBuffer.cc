#include "AudioBuffer.h"

size_t AudioBuffer::getSampleSizeInBytes(AudioBuffer::SampleFormat pf) noexcept
{
    switch (pf) {
        case AudioBuffer::SampleFormat::SINT32:
            return sizeof(int32_t);

        case AudioBuffer::SampleFormat::DOUBLE:
            return sizeof(double);

        case AudioBuffer::SampleFormat::FLOAT:
            return sizeof(float);
    }

    // this MUST NOT happen
    return 0;
}

AudioBuffer::AudioBuffer(AudioBuffer::SampleFormat sf, uint16_t channelsCount, uint32_t samplesCount) noexcept :
    BufferBackedData(), m_SampleFormat(sf), m_ChannelsCount(channelsCount), m_SamplesCount(samplesCount)
{
}

AudioBuffer::AudioBuffer(AudioBuffer&& src) noexcept :
    BufferBackedData(std::move(src)), m_SampleFormat(src.m_SampleFormat), m_ChannelsCount(src.m_ChannelsCount), m_SamplesCount(src.m_SamplesCount)
{
}

AudioBuffer& AudioBuffer::operator=(AudioBuffer&& src) noexcept
{
    if (this != &src) {
        BufferBackedData::operator=(std::move(src));
        m_SampleFormat  = src.m_SampleFormat;
        m_ChannelsCount = src.m_ChannelsCount;
        m_SamplesCount  = src.m_SamplesCount;
    }

    return *this;
}

AudioBuffer::~AudioBuffer() {}

AudioBuffer::SampleFormat AudioBuffer::getSampleFormat() const noexcept
{
    return m_SampleFormat;
}

uint16_t AudioBuffer::getChannelsCount() const noexcept
{
    return m_ChannelsCount;
}

uint32_t AudioBuffer::getSamplesCount() const noexcept
{
    return m_SamplesCount;
}

void AudioBuffer::storeAudioData(const MallocFunctionType& mallocFn, const DeallocFunctionType& deallocFn, const std::function<void(void*)>& fillerFn) noexcept
{
    const size_t sampleSize    = getSampleSizeInBytes(getSampleFormat());
    const size_t requiredBytes = sampleSize * getChannelsCount() * getSamplesCount();

    this->storeData(requiredBytes, mallocFn, deallocFn, fillerFn);
}