#include "FFMPEGDecoder.h"

#include <filesystem>

// for memcpy
#include <cstring>

#include <chrono>
using namespace std::chrono;

// ffmpeg
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavcodec/avfft.h>
#include <libavdevice/avdevice.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavformat/avformat.h>
#include <libavutil/avassert.h>
#include <libavutil/avstring.h>
#include <libavutil/bprint.h>
#include <libavutil/dict.h>
#include <libavutil/display.h>
#include <libavutil/eval.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/opt.h>
#include <libavutil/parseutils.h>
#include <libavutil/pixdesc.h>
#include <libavutil/samplefmt.h>
#include <libavutil/time.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
}

void writeDataToFrameMemory(void* dst, AVFrame* avFrame, int width, int height) noexcept
{
    const auto bytesForAPixel = Frame::getPixelSizeInBytes(Frame::PixelFormat::RGBA64);

    for (int y = 0; y < height; ++y) {
        if (avFrame->linesize[0] > 0) {

            std::copy(
                avFrame->data[0] + (y * avFrame->linesize[0]),
                avFrame->data[0] + (y * avFrame->linesize[0]) + width * bytesForAPixel,
                reinterpret_cast<uint8_t*>(dst) + (y * width * bytesForAPixel)
            );
        } else {
            // TODO: will point to the last byte in row
        }
    }
}

FFMPEGDecoder::FFMPEGDecoder(FrameStreamHandler& queueHandler, const Frame::MallocFunctionType& allocate, const Frame::DeallocFunctionType& deallocate) noexcept
    :
    Decoder(queueHandler, allocate, deallocate),
    m_FFMPEGThread([this]() -> void { return decode(); }), m_ShouldClose(false), m_LoadFileRequested(false), m_PlayRequested(false), m_PauseRequested(false),
    m_ResumeRequested(false), m_JumpRequested(false), m_StopRequested(false)
{
}

FFMPEGDecoder::~FFMPEGDecoder()
{
    m_ShouldClose = true;
    m_FFMPEGThread.join();
}

void FFMPEGDecoder::loadFile(const Decoder::FileNameType& filename) noexcept
{
    m_LoadedFilename    = filename;
    m_LoadFileRequested = true;
}

void FFMPEGDecoder::play() noexcept
{
    m_PlayRequested = true;
}

void FFMPEGDecoder::stop() noexcept
{
    m_StopRequested = true;
}

void FFMPEGDecoder::pause() noexcept
{
    m_PauseRequested = true;
}

void FFMPEGDecoder::resume() noexcept
{
    m_ResumeRequested = true;
}

void FFMPEGDecoder::seek(double time) noexcept
{
    m_JumpTo        = time;
    m_JumpRequested = true;
}

AudioBuffer::SampleFormat getSampleFormatFromFFMPEGFormat(AVSampleFormat sf) noexcept
{
    AudioBuffer::SampleFormat sampleFormat = AudioBuffer::SampleFormat::SINT32;

    switch (sf) {
        case AV_SAMPLE_FMT_S32P:
            sampleFormat = AudioBuffer::SampleFormat::SINT32;
            break;
        case AV_SAMPLE_FMT_DBL:
            sampleFormat = AudioBuffer::SampleFormat::DOUBLE;
            break;
        case AV_SAMPLE_FMT_FLT:
            sampleFormat = AudioBuffer::SampleFormat::FLOAT;
            break;
        default:
            sampleFormat = AudioBuffer::SampleFormat::SINT32;
    }

    return sampleFormat;
}

void FFMPEGDecoder::decode() noexcept
{
    if (!startDecoding()) {
        return;
    }

    DecoderStatus currentStatus = DecoderStatus::WaitingForFile;

    setStatus(currentStatus);

    AVFormatContext* pFormatCtx = nullptr;

    int ret = -1, videoStream = -1, audioStream = -1;

    // The stream's information about the codec is in what we call the
    // "codec context." This contains all the information about the codec
    // that the stream is using
    AVCodecContext* pCodecCtx      = nullptr;
    AVCodecContext* pAudioCodecCtx = nullptr;

    const AVCodec* pCodec      = nullptr;
    const AVCodec* pAudioCodec = nullptr;

    AVFrame* pFrame    = nullptr;
    AVFrame* pFrameRGB = nullptr;

    struct SwsContext* sws_ctx = nullptr;
    struct SwrContext* swr_ctx = nullptr;

    AVPacket* pPacket = nullptr;

    // Even though we've allocated the frame, we still need a place to put
    // the raw data when we convert it. We use avpicture_get_size to get
    // the size we need, and allocate the space manually:
    uint8_t* buffer = nullptr;
    int      numBytes;

    AVRational videoTimeBaseInSeconds, audioTimeBaseInSeconds;

    uint16_t       audioOutputChannels   = 1;
    AVSampleFormat audioOutputFormat     = AV_SAMPLE_FMT_S32P;
    int            audioOutputSampleRate = 44100;

    std::vector<uint8_t>  audioOutputBuffer;
    std::vector<uint8_t*> outputChannels(0);

    auto open = [&](const char* filename) {
        if (!std::filesystem::exists(filename)) {
            // file doesn't exist
            std::cerr << "The file " << filename << " doesn't exist" << std::endl;

            return -1;
        }

        // now we can actually open the file:
        // the minimum information required to open a file is its URL, which is
        // passed to avformat_open_input(), as in the following code:
        ret = avformat_open_input(&pFormatCtx, filename, nullptr, nullptr); // [2]
        if (ret < 0) {
            // couldn't open file
            std::cerr << "Could not open file " << filename << std::endl;

            // exit with error
            return -1;
        }

        // The call to avformat_open_input(), only looks at the header, so next
        // we need to check out the stream information in the file.: Retrieve
        // stream information
        ret = avformat_find_stream_info(pFormatCtx, nullptr); //[3]
        if (ret < 0) {
            // couldn't find stream information
            std::cerr << "Could not find stream information " << filename << std::endl;

            // exit with error
            return -2;
        }

        // We introduce a handy debugging function to show us what's inside
        // dumping information about file onto standard error
        av_dump_format(pFormatCtx, 0, filename, 0);

        // Now pFormatCtx->streams is just an array of pointers, of size
        // pFormatCtx->nb_streams, so let's walk through it until we find a
        // video stream.
        int i = 0;

        // Find the first video stream and reset value "videoStream" and "audioStream"
        videoStream = -1;
        audioStream = -1;
        for (i = 0; i < pFormatCtx->nb_streams; i++) {
            const auto currentStreamType = pFormatCtx->streams[i]->codecpar->codec_type;
            // check the General type of the encoded data to match
            // AVMEDIA_TYPE_VIDEO
            if (currentStreamType == AVMEDIA_TYPE_VIDEO) // [5]
            {
                videoStream = i;
            } else if (currentStreamType == AVMEDIA_TYPE_AUDIO) {
                audioStream = i;
            }
        }

        if (videoStream == -1) {
            // didn't find a video stream
            return -3;
        }

        // New API.
        // This implementation uses the new API.
        // Please refer to tutorial01-deprecated.c for an implementation using
        // the deprecated FFmpeg API.

        // Find the decoder for the video stream
        pCodec = avcodec_find_decoder(pFormatCtx->streams[videoStream]->codecpar->codec_id);
        if (pCodec == nullptr) {
            // codec not found
            std::cerr << "Unsupported codec for " << filename << std::endl;

            // exit with error
            return -4;
        }

        videoTimeBaseInSeconds = pFormatCtx->streams[videoStream]->time_base;

        if (audioStream >= 0) {
            audioTimeBaseInSeconds = pFormatCtx->streams[audioStream]->time_base;

            // Find the decoder for the audio stream
            pAudioCodec = avcodec_find_decoder(pFormatCtx->streams[audioStream]->codecpar->codec_id);

            pAudioCodecCtx = avcodec_alloc_context3(pAudioCodec);
            ret            = avcodec_parameters_to_context(pAudioCodecCtx, pFormatCtx->streams[audioStream]->codecpar);
            if (ret != 0) {
                // error copying codec context
                std::cerr << "Could not copy codec context for " << filename << std::endl;

                // exit with error
                return -50;
            }

            if (avcodec_open2(pAudioCodecCtx, pAudioCodec, NULL) < 0) {
                // codec not found
                std::cerr << "Unsupported codec for " << filename << std::endl;

                // exit with error
                return -40;
            } else {
                swr_ctx = swr_alloc();
                av_opt_set_int(swr_ctx, "in_channel_count", pAudioCodecCtx->channels, 0);
                av_opt_set_int(swr_ctx, "out_channel_count", 1, 0);
                av_opt_set_int(swr_ctx, "in_channel_layout", pAudioCodecCtx->channel_layout, 0);
                av_opt_set_int(swr_ctx, "out_channel_layout", AV_CH_LAYOUT_MONO, 0);
                av_opt_set_int(swr_ctx, "in_sample_rate", pAudioCodecCtx->sample_rate, 0);
                av_opt_set_int(swr_ctx, "out_sample_rate", audioOutputSampleRate, 0);
                av_opt_set_sample_fmt(swr_ctx, "in_sample_fmt", pAudioCodecCtx->sample_fmt, 0);
                av_opt_set_sample_fmt(swr_ctx, "out_sample_fmt", audioOutputFormat, 0);
                swr_init(swr_ctx);

                // The first audioOutputChannels is for number of output channels, change that when changing AV_CH_LAYOUT_MONO
                audioOutputBuffer.resize(audioOutputChannels * MaxAudioBufferSizePerChannel);

                outputChannels.clear();
                for (size_t ac = 0; ac < audioOutputChannels; ++ac) {
                    outputChannels.push_back(&audioOutputBuffer[MaxAudioBufferSizePerChannel * ac]);
                }

                if (!swr_is_initialized(swr_ctx)) {
                    fprintf(stderr, "Resampler has not been properly initialized\n");
                    return -1;
                }
            }
        }

        pCodecCtx = avcodec_alloc_context3(pCodec);
        ret       = avcodec_parameters_to_context(pCodecCtx, pFormatCtx->streams[videoStream]->codecpar);
        if (ret != 0) {
            // error copying codec context
            std::cerr << "Could not copy codec context for " << filename << std::endl;

            // exit with error
            return -5;
        }

        // Open codec
        ret = avcodec_open2(pCodecCtx, pCodec, nullptr); // [8]
        if (ret < 0) {
            // Could not open codec
            std::cerr << "Could not open codec for " << filename << std::endl;

            // exit with error
            return -6;
        }

        // Allocate video frame
        pFrame = av_frame_alloc(); // [9]
        if (!pFrame) {
            // Could not allocate frame
            std::cerr << "Could not allocate frame for " << filename << std::endl;

            // exit with error
            return -7;
        }

        // Allocate an AVFrame structure
        pFrameRGB = av_frame_alloc();
        if (!pFrameRGB) {
            // Could not allocate frame
            std::cerr << "Could not allocate frame for " << filename << std::endl;

            // exit with error
            return -8;
        }

        // Determine required buffer size and allocate buffer
        // numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width,
        // pCodecCtx->height); // changed to AV_PIX_FMT_AYUV64LE
        // https://ffmpeg.org/pipermail/ffmpeg-devel/2016-January/187299.html
        // what is 'linesize alignment' meaning?:
        // https://stackoverflow.com/questions/35678041/what-is-linesize-alignment-meaning
        numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGBA64, pCodecCtx->width, pCodecCtx->height, 64);
        buffer   = (uint8_t*)av_malloc(numBytes * sizeof(uint8_t));

        //
        // Now we use avpicture_fill() to associate the frame with our newly
        // allocated buffer. About the AVPicture cast: the AVPicture struct is
        // a subset of the AVFrame struct - the beginning of the AVFrame struct
        // is identical to the AVPicture struct.
        //

        // Assign appropriate parts of buffer to image planes in pFrameRGB
        // Note that pFrameRGB is an AVFrame.
        av_image_fill_arrays( // [12]
            pFrameRGB->data,
            pFrameRGB->linesize,
            buffer,
            AV_PIX_FMT_RGBA64,
            pCodecCtx->width,
            pCodecCtx->height,
            32
        );

        // Finally! Now we're ready to read from the stream!

        //
        // What we're going to do is read through the entire video stream by
        // reading in the packet, decoding it into our frame, and once our
        // frame is complete, we will convert and save it.
        //

        pPacket = av_packet_alloc();
        if (!pPacket) {
            // couldn't allocate packet
            std::cerr << "Could not allocate packet for " << filename << std::endl;

            // exit with error
            return -9;
        }

        // initialize SWS context for software scaling
        sws_ctx = sws_getContext( // [13]
            pCodecCtx->width,
            pCodecCtx->height,
            pCodecCtx->pix_fmt,
            pCodecCtx->width,
            pCodecCtx->height,
            AV_PIX_FMT_RGBA64, // sws_scale destination color scheme
            SWS_BILINEAR,
            nullptr,
            nullptr,
            nullptr
        );

        //
        // The process, again, is simple: av_read_frame() reads in a packet and
        // stores it in the AVPacket struct. Note that we've only allocated the
        // packet structure - ffmpeg allocates the internal data for us, which
        // is pointed to by packet.data. This is freed by the av_free_packet()
        // later. avcodec_decode_video() converts the packet to a frame for us.
        // However, we might not have all the information we need for a frame
        // after decoding a packet, so avcodec_decode_video() sets
        // frameFinished for us when we have decoded enough packets the next
        // frame.
        // Finally, we use sws_scale() to convert from the native format
        // (pCodecCtx->pix_fmt) to RGB. Remember that you can cast an AVFrame
        // pointer to an AVPicture pointer. Finally, we pass the frame and
        // height and width information to our SaveFrame function.
        //

        return 0;
    };

    auto cleanup = [&]() {
        if (pPacket != nullptr) {
            av_packet_free(&pPacket);
            pPacket = nullptr;
        }

        if (sws_ctx != nullptr) {
            sws_freeContext(sws_ctx);
            sws_ctx = nullptr;
        }

        if (swr_ctx != nullptr) {
            swr_close(swr_ctx);
            swr_free(&swr_ctx);
            swr_ctx = nullptr;
        }

        // Free the RGB buffer
        if (buffer != nullptr) {
            av_free(buffer);
            buffer = nullptr;
        }

        // free the RGB image structure
        if (pFrameRGB != nullptr) {
            av_frame_free(&pFrameRGB);
            av_free(pFrameRGB);
            pFrameRGB = nullptr;
        }

        // Free the last decoded frame
        if (pFrame != nullptr) {
            av_frame_free(&pFrame);
            av_free(pFrame);
        }

        // Close codecs
        if (pCodecCtx != nullptr) {
            avcodec_close(pCodecCtx);
            pCodecCtx = nullptr;
        }

        if (pAudioCodecCtx != nullptr) {
            avcodec_close(pAudioCodecCtx);
            pAudioCodecCtx = nullptr;
        }

        // Close the video file
        if (pFormatCtx != nullptr) {
            avformat_close_input(&pFormatCtx);
            pFormatCtx = nullptr;
        }
    };

    auto decode_packet = [&]() {
        if (av_read_frame(pFormatCtx, pPacket) < 0) { // [14]
            return -11;
        }

        /*
        AVRational rescaledTimeBaseInSeconds;
        rescaledTimeBaseInSeconds.num = 1;
        rescaledTimeBaseInSeconds.den = 1000000;

        av_packet_rescale_ts(pPacket, videoTimeBaseInSeconds, rescaledTimeBaseInSeconds);
        */

        // Is this a packet from the video stream?
        if (pPacket->stream_index == videoStream) {
            // Decode video frame
            // avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished,
            // &pPacket); Deprecated: Use avcodec_send_packet() and
            // avcodec_receive_frame().
            ret = avcodec_send_packet(pCodecCtx, pPacket); // [15]
            if (ret < 0) {
                // could not send packet for decoding
                printf("Error sending packet for decoding.\n");

                // exit with eror
                return -10;
            }

            while (ret >= 0) {
                ret = avcodec_receive_frame(pCodecCtx, pFrame); // [15]

                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    // EOF exit loop
                    break;
                } else if (ret < 0) {
                    // could not decode packet
                    printf("Error while decoding.\n");

                    // exit with error
                    return -11;
                }

                // Convert the image from its native format to RGB
                sws_scale( // [16]
                    sws_ctx,
                    (uint8_t const* const*)pFrame->data,
                    pFrame->linesize,
                    0,
                    pCodecCtx->height,
                    pFrameRGB->data,
                    pFrameRGB->linesize
                );

                // using pFrame->best_effort_timestamp to set playback timing
                // info as pFrameRGB contains the correct pixel data but the
                // wrong timestamp

                // send frame to FrameCollection
                emitFrame(
                    Frame::PixelFormat::RGBA64,
                    pCodecCtx->width,
                    pCodecCtx->height,
                    videoTimeBaseInSeconds.num,
                    videoTimeBaseInSeconds.den,
                    pFrame->best_effort_timestamp,
                    [&](void* frameMemory) { writeDataToFrameMemory(frameMemory, pFrameRGB, pCodecCtx->width, pCodecCtx->height); }
                );
            }
        } else if ((audioStream >= 0) && (pPacket->stream_index == audioStream)) {
            // Decode audio frame
            // avcodec_decode_audio4 has been deprecated: Use avcodec_send_packet() and
            // avcodec_receive_frame().
            ret = avcodec_send_packet(pAudioCodecCtx, pPacket);
            if (ret < 0) {
                // could not send packet for decoding
                printf("Error sending packet for decoding.\n");

                // exit with error
                return -10;
            }

            while (ret >= 0) {
                ret = avcodec_receive_frame(pAudioCodecCtx, pFrame); // [15]

                /*
                int linesize = 0;
                int dataSize = av_samples_get_buffer_size(&linesize, pAudioCodecCtx->ch_layout.nb_channels, pFrame->nb_samples, pAudioCodecCtx->sample_fmt, 1);

                int outSize = av_samples_get_buffer_size(nullptr, audioOutputChannels, pFrame->nb_samples, audioOutputFormat, 1);
                */

                const AudioBuffer::SampleFormat sampleFormat        = getSampleFormatFromFFMPEGFormat(audioOutputFormat);
                const auto                      sizeOfSampleInBytes = AudioBuffer::getSampleSizeInBytes(sampleFormat);

                // returns the number of samples per channel in one audio frame  -- 8192
                int convertedSamples = swr_convert(
                    swr_ctx,
                    outputChannels.data(),
                    MaxAudioBufferSizePerChannel / sizeOfSampleInBytes,
                    (const uint8_t**)&pFrame->data[0], // input
                    pFrame->nb_samples
                );

                if (convertedSamples > 0) {
                    emitAudioBuffer(
                        sampleFormat,
                        audioOutputChannels,
                        convertedSamples,
                        audioTimeBaseInSeconds.num,
                        audioTimeBaseInSeconds.den,
                        pFrame->best_effort_timestamp,
                        [&](void* buffer) {
                            auto startOfNextData = reinterpret_cast<uint8_t*>(buffer);
                            for (auto& outputChannel : outputChannels) {
                                startOfNextData = std::copy(&outputChannel[0], &outputChannel[convertedSamples * sizeOfSampleInBytes], startOfNextData);
                            }
                        }
                    );
                }
            }
        }

        // Free the packet that was allocated by av_read_frame
        // [FFmpeg-cvslog] avpacket: Replace av_free_packet with
        // av_packet_unref
        // https://lists.ffmpeg.org/pipermail/ffmpeg-cvslog/2015-October/094920.html
        av_packet_unref(pPacket);

        return 0;
    };

    while (!m_ShouldClose) {
        // first thing to do is to get the current status
        currentStatus = getStatus();

        switch (currentStatus) {
            case DecoderStatus::WaitingForFile:
                {
                    if (m_LoadFileRequested) {

                        // create an empty stream and set the old one as the new
                        // one in the stopped status
                        createEmptyStream(DecoderStatus::WaitingForFile, DecoderStatus::Stopped);

                        // open the provided file initializing the decoding
                        if (open(m_LoadedFilename.c_str()) >= 0) {
                            // the new status will be waiting for play...
                            setStatus(DecoderStatus::WaitingForPlay);
                        } else {
                            // clean up used memory for another open
                            cleanup();

                            // If the file has not been found or in case of other error such as file damaged I report the status of the player to
                            // "WaitingForFile"
                            setStatus(DecoderStatus::WaitingForFile);
                        }

                        // next while execution this won't be executed with the
                        // same value
                        m_LoadFileRequested = false;
                    }
                }
                break;

            case DecoderStatus::WaitingForPlay:
                {
                    if (m_PlayRequested) {
                        // the new status will be active...
                        setStatus(DecoderStatus::Active);

                        m_PlayRequested = false;
                    } else if (m_JumpRequested) {
                        // Calculate the exact time to go
                        auto time = m_JumpTo * double(AV_TIME_BASE_Q.den) / double(AV_TIME_BASE_Q.num);

                        // Execute the seek (search)
                        if (av_seek_frame(pFormatCtx, -1, time, AVSEEK_FLAG_ANY) >= 0) {
                            // create an empty stream and set the old to stopped
                            // status and the one in the active status
                            createEmptyStream(DecoderStatus::Active, DecoderStatus::Seeking);

                            // Execute the flush of buffer
                            avcodec_flush_buffers(pCodecCtx);
                        }

                        // Stop the seek (search)
                        m_JumpRequested = false;
                    } else {
                        // I reset the variables if they are still paused, to avoid launching unwanted commands
                        m_LoadFileRequested = false;
                        m_PauseRequested    = false;
                        m_ResumeRequested   = false;
                    }
                }
                break;

            case DecoderStatus::Stopped:
                {
                    if (m_LoadFileRequested) {
                        // the new status will be stopped...
                        setStatus(DecoderStatus::WaitingForPlay);

                        // create an empty stream and set the old one as the new
                        // one in the stopped status
                        createEmptyStream(DecoderStatus::WaitingForPlay, DecoderStatus::Stopped);

                        // open the provided file initializing the decoding
                        if (open(m_LoadedFilename.c_str()) >= 0) {
                            // the new status will be waiting for play...
                            setStatus(DecoderStatus::WaitingForPlay);
                        } else {
                            // clean up used memory for another open
                            cleanup();

                            // If the file has not been found or in case of other error such as file damaged I report the status of the player to
                            // "WaitingForFile"
                            setStatus(DecoderStatus::WaitingForFile);
                        }

                        // next while execution this won't be executed with the
                        // same value
                        m_LoadFileRequested = false;
                    } else {
                        // I reset the variables if they are still paused, to avoid launching unwanted commands
                        m_PlayRequested   = false;
                        m_PauseRequested  = false;
                        m_ResumeRequested = false;
                        m_JumpRequested   = false;
                    }
                }
                break;

            case DecoderStatus::Active:
                {
                    if (m_LoadFileRequested) {
                        // clean the current decode context
                        cleanup();

                        // the new status will be waiting for play...
                        setStatus(DecoderStatus::Stopped);

                        // create an empty stream and set the old one as the new
                        // one in the waiting for play status
                        createEmptyStream(DecoderStatus::WaitingForPlay);

                        // open the provided file initializing the decoding
                        if (open(m_LoadedFilename.c_str()) >= 0) {
                            // the new status will be waiting for play...
                            setStatus(DecoderStatus::WaitingForPlay);
                        } else {
                            // clean up used memory for another open
                            cleanup();

                            // If the file has not been found or in case of other error such as file damaged I report the status of the player to
                            // "WaitingForFile"
                            setStatus(DecoderStatus::WaitingForFile);
                        }

                        // next while execution this won't be executed with the
                        // same value
                        m_LoadFileRequested = false;
                    } else if (m_PauseRequested) {
                        setStatus(DecoderStatus::Paused);

                        m_PauseRequested = false;
                    } else if (m_JumpRequested) {
                        // Calculate the exact time to go
                        auto time = m_JumpTo * double(AV_TIME_BASE_Q.den) / double(AV_TIME_BASE_Q.num);

                        // Execute the seek (search)
                        if (av_seek_frame(pFormatCtx, -1, time, AVSEEK_FLAG_ANY) >= 0) {
                            // create an empty stream and set the old to stopped
                            // status and the one in the active status
                            createEmptyStream(DecoderStatus::Active, DecoderStatus::Seeking);

                            // Execute the flush of buffer
                            avcodec_flush_buffers(pCodecCtx);
                        }

                        // Stop the seek (search)
                        m_JumpRequested = false;
                    } else if (m_StopRequested) {
                        cleanup();

                        setStatus(DecoderStatus::Stopped);

                        m_StopRequested = false;
                    } else {
                        if (countFramesInFlight() < MaxVideoDataInFlight) {
                            if (decode_packet() < 0) {
                                cleanup();

                                setStatus(DecoderStatus::Stopped);
                                // Clean the stream to leave no residue
                                createEmptyStream(DecoderStatus::Stopped, DecoderStatus::Seeking);

                                m_StopRequested = false;
                            }
                        } else {
                            // let's take a video a super fast video at 120 fps,
                            // it's a bit more than 8ms for a single frame...
                            // sleeping for 8ms in this thread that does not
                            // perform blocking operations unless requested by
                            // the user won't impact the playback as to work
                            // correctly decoding a frame MUST take less time
                            // than display it in the first place. Moreover
                            // putting the thread to sleep for at least 8
                            // milliseconds will give more execution time to the
                            // playback thread thus reducing the overall
                            // pressure on the CPU and reduce the lag
                            // probability that SHOULD already be extremely low.
                            std::this_thread::sleep_for(std::chrono::milliseconds(8));
                        }
                        m_PlayRequested   = false;
                        m_ResumeRequested = false;
                    }
                }
                break;

            case DecoderStatus::Paused:
                {
                    if (m_ResumeRequested) {
                        setStatus(DecoderStatus::Active);

                        m_ResumeRequested = false;
                    } else if (m_LoadFileRequested) {
                        // clean the current decode context
                        cleanup();

                        // the new status will be waiting for play...
                        setStatus(DecoderStatus::WaitingForPlay);

                        // create an empty stream and set the old one as the new
                        // one in the waiting for play status
                        createEmptyStream(DecoderStatus::WaitingForPlay, DecoderStatus::Seeking);

                        // open the provided file initializing the decoding
                        open(m_LoadedFilename.c_str());

                        // next while execution this won't be executed with the
                        // same value
                        m_LoadFileRequested = false;
                    } else if (m_JumpRequested) {
                        // Calculate the exact time to go
                        auto time = m_JumpTo * double(AV_TIME_BASE_Q.den) / double(AV_TIME_BASE_Q.num);

                        // Execute the seek (search)
                        if (av_seek_frame(pFormatCtx, -1, time, AVSEEK_FLAG_ANY) >= 0) {
                            // create an empty stream and set the old to stopped
                            // status and the one in the active status
                            createEmptyStream(DecoderStatus::Active, DecoderStatus::Seeking);

                            // Execute the flush of buffer
                            avcodec_flush_buffers(pCodecCtx);
                        }

                        // Stop the seek (search)
                        m_JumpRequested = false;
                    } else {
                        // I reset the variables if they are still paused, to avoid launching unwanted commands
                        m_PlayRequested  = false;
                        m_PauseRequested = false;
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    }
                }
                break;
        }
    }

    // avoid memory leaks
    cleanup();

    endDecoding();
}
