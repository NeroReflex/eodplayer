#include "VideoData.h"

VideoData::VideoData(VideoData&& src) noexcept :
    m_TimeBaseNum(src.m_TimeBaseNum), m_TimeBaseDen(src.m_TimeBaseDen), m_TimeTicks(src.m_TimeTicks), m_Frame(std::move(src.m_Frame)),
    m_AudioBuffer(std::move(src.m_AudioBuffer))
{
}

VideoData::VideoData(int64_t timebase_num, int64_t timebase_den, int64_t timestamp) noexcept :
    m_TimeBaseNum(timebase_num), m_TimeBaseDen(timebase_den), m_TimeTicks(timestamp)
{
}

void VideoData::emplaceFrame(Frame&& frame) noexcept
{
    m_Frame = std::move(frame);
}

void VideoData::emplaceAudioBuffer(AudioBuffer&& audio) noexcept
{
    m_AudioBuffer = std::move(audio);
}

VideoData& VideoData::operator=(VideoData&& src) noexcept
{
    if (&src != this) {
        m_TimeBaseNum = src.m_TimeBaseNum;
        m_TimeBaseDen = src.m_TimeBaseDen;
        m_TimeTicks   = src.m_TimeTicks;
        m_Frame       = std::move(src.m_Frame);
        m_AudioBuffer = std::move(src.m_AudioBuffer);
    }

    return *this;
}

int64_t VideoData::getTimeBaseNum() const noexcept
{
    return m_TimeBaseNum;
}

int64_t VideoData::getTimeBaseDen() const noexcept
{
    return m_TimeBaseDen;
}

int64_t VideoData::getTimeTicks() const noexcept
{
    return m_TimeTicks;
}

bool VideoData::hasFrame() const noexcept
{
    return m_Frame.has_value();
}

const Frame& VideoData::getFrame() const noexcept
{
    return m_Frame.value();
}

bool VideoData::hasAudio() const noexcept
{
    return m_AudioBuffer.has_value();
}

const AudioBuffer& VideoData::getAudioBuffer() const noexcept
{
    return m_AudioBuffer.value();
}