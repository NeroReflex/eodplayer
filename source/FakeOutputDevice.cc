#include "FakeOutputDevice.h"

FakeOutputDevice::FakeOutputDevice(FrameStreamHandler& queueHandler) noexcept : OutputDevice(queueHandler) {}

FakeOutputDevice::~FakeOutputDevice() {}

void FakeOutputDevice::exec() noexcept
{
    while (true) {
        auto possibly_frame = receiveVideoData();
        if (possibly_frame.has_value()) {
            // std::cout << "got a frame!" << std::endl;
        } else {
            // std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}
