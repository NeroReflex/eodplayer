#include "FileOutputDevice.h"

#include <fstream>

FileOutputDevice::FileOutputDevice(FrameStreamHandler& queueHandler) noexcept : OutputDevice(queueHandler) {}

FileOutputDevice::~FileOutputDevice() {}

void FileOutputDevice::exec() noexcept
{
    uint32_t frameIndex = 0;

    while (true) {
        auto possibly_frame = receiveVideoData();
        if ((possibly_frame.has_value()) && (possibly_frame.value().hasFrame())) {
            // write frame to disk in two separates files: one for alpha and one
            // for colors
            std::string colorFilename = std::string("frame_") + std::to_string(frameIndex) + std::string("_rgb.pnm");
            std::string alphaFilename = std::string("frame_") + std::to_string(frameIndex) + std::string("_a.pnm");

            if (frameIndex < 60) {
                writeFrame(colorFilename, alphaFilename, std::move(possibly_frame.value().getFrame()));
            }

            ++frameIndex;
        } else {
            // TODO: implement
        }
    }
}

void FileOutputDevice::writeFrame(const std::string& colorFilename, const std::string& alphaFilename, const Frame& frame) const noexcept
{
    if (!frame.isHoldingData()) {
        std::cout << "No data on frame" << std::endl;
        return;
    }

    std::fstream colorFile(colorFilename, std::fstream::out | std::fstream::binary);

    if (!colorFile) {
        std::cout << "Couldn't open " << colorFilename << " file in write mode" << std::endl;
        return;
    }

    std::fstream alphaFile(alphaFilename, std::fstream::out | std::fstream::binary);

    if (!colorFile) {
        std::cout << "Couldn't open " << alphaFilename << " file in write mode" << std::endl;
        return;
    }

    const auto width  = frame.getWidth();
    const auto height = frame.getHeight();

    // output PPM header
    colorFile << "P6\n" << std::to_string(width) << " " << std::to_string(height) << "\n65535\n";
    alphaFile << "P6\n" << std::to_string(width) << " " << std::to_string(height) << "\n65535\n";

    for (uint32_t h = 0; h < frame.getHeight(); ++h) {
        for (uint32_t w = 0; w < frame.getWidth(); ++w) {
            switch (frame.getPixelFormat()) {
                case Frame::PixelFormat::RGBA64:
                    uint16_t* pixel = reinterpret_cast<uint16_t*>(reinterpret_cast<uint8_t*>(frame.getRawBuffer()) + (((h * width) + w) * 8));

                    colorFile.write(reinterpret_cast<char*>(&pixel[0]), 2);
                    colorFile.write(reinterpret_cast<char*>(&pixel[1]), 2);
                    colorFile.write(reinterpret_cast<char*>(&pixel[2]), 2);
                    alphaFile.write(reinterpret_cast<char*>(&pixel[3]), 2);
                    alphaFile.write(reinterpret_cast<char*>(&pixel[3]), 2);
                    alphaFile.write(reinterpret_cast<char*>(&pixel[3]), 2);

                    break;
            }
        }
    }

    // close files
    colorFile.close();
    alphaFile.close();
}
