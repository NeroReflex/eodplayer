#include "FFMPEGDecoder.h"
#include "FakeOutputDevice.h"
#include "FileOutputDevice.h"
#include "FrameStreams.h"
#include "OpenGLOutputDevice.h"

#include "NetworkCommandWatcher.h"

#include "Commands/DecoderCommand.h"

#include <queue>

std::allocator<uint8_t> frameMemoryManagerObj;
const static auto       frameMemoryAllocator = [](size_t sz) -> void* {
    void* malloc_result = frameMemoryManagerObj.allocate(sz + sizeof(size_t));

    if (malloc_result) {
        *(reinterpret_cast<size_t*>(malloc_result)) = sz;

        // allocated += sz;
    }

    return reinterpret_cast<void*>(reinterpret_cast<char*>(malloc_result) + sizeof(size_t));
};

const static auto frameMemoryDeallocator = [](void* mem) -> void {
    uint8_t* orig_ptr = reinterpret_cast<uint8_t*>(mem) - sizeof(size_t);

    const auto sz = *(reinterpret_cast<uint32_t*>(orig_ptr));

    return frameMemoryManagerObj.deallocate(orig_ptr, sz);
};

/**
 * Entry point.
 *
 * @param   argc    command line arguments counter.
 * @param   argv    command line arguments.
 *
 * @return          execution exit code.
 */
int main(int argc, char* argv[])
{
    const auto initResult = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS);
    if (initResult != 0) {
        std::cerr << "Error initializing SDL2" << std::endl;

        return EXIT_FAILURE;
    }

    // this is the thread-safe double ended queue used by components that must
    // enqueue and dequeue frames
    std::allocator<FrameStreamHandler::FrameDequeue> allocator;
    FrameStreams                                     frameQueuesHandler(allocator);

    DequeHM<DecoderCommand*> commandQueue;
    NetworkCommandWatcher    commandQueueFiller(commandQueue);
    const auto               shutdownCommandQueueFillerOrEmpty = commandQueueFiller.run();

    if (!shutdownCommandQueueFillerOrEmpty.has_value()) {
        std::cerr << "Error in setting up the command watcher" << std::endl;
        return EXIT_FAILURE;
    }

    std::atomic_bool closeCommandArrived = false;

    std::thread outputHandlerThread = std::thread([&]() {
        // auto debugOutput = new FakeBufferedFrameOutputDevice(8);
        // std::unique_ptr<OutputDevice> debugOutput =
        // std::make_unique<FileOutputDevice>(frameQueuesHandler);
        std::unique_ptr<OutputDevice> debugOutput = std::make_unique<OpenGLOutputDevice>(frameQueuesHandler, 1920 * 2, 1080);

        // this is a blocking call
        debugOutput->run();

        debugOutput.reset();

        closeCommandArrived = true;
    });

    auto shutdownCommandQueueFillerFn = shutdownCommandQueueFillerOrEmpty.value();

    std::thread decoderHandlerThread = std::thread([&]() {
        auto decoder = std::make_unique<FFMPEGDecoder>(frameQueuesHandler, frameMemoryAllocator, frameMemoryDeallocator);

        while (!closeCommandArrived) {
            const auto possibleCommand = commandQueue.pop_front();

            if (possibleCommand.has_value()) {
                auto command = possibleCommand.value();

                command->execute(decoder.get());

                delete command;
            } else {
                // give a break to the CPU by sleeping an amount of time that the user SHOULD NOT and HOPEFULLY WILL NOT notice
                std::this_thread::sleep_for(std::chrono::milliseconds(2));
            }
        }

        decoder.reset();
    });

    outputHandlerThread.join();
    shutdownCommandQueueFillerFn();
    decoderHandlerThread.join();

    return EXIT_SUCCESS;
}
