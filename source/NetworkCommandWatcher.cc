#include "NetworkCommandWatcher.h"

#define PORT    8080
#define MAXLINE 1024

NetworkCommandWatcher::NetworkCommandWatcher(DequeHM<DecoderCommand*>& commandsQueue) noexcept :
    CommandReceiver(commandsQueue), m_NetworkHandlerThread(nullptr), m_ShouldStop(false), m_Socket(-1)
{
}

NetworkCommandWatcher::~NetworkCommandWatcher()
{
    reset();
}

void NetworkCommandWatcher::reset() noexcept
{
    if (m_NetworkHandlerThread) {
        m_ShouldStop = true;

        m_NetworkHandlerThread->join();

        m_NetworkHandlerThread.reset();
    }
}

std::optional<std::function<void(void)>> NetworkCommandWatcher::run() noexcept
{
    m_Socket = -1;

    // Creating socket file descriptor
    if ((m_Socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::cerr << "socket creation failed" << std::endl;

        return std::optional<std::function<void(void)>>();
    }

    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(PORT);
    servaddr.sin_addr.s_addr = INADDR_ANY;

    struct timeval read_timeout;
    read_timeout.tv_sec  = 0;
    read_timeout.tv_usec = 10;
    setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);

    if (bind(m_Socket, (const struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
        std::cerr << "bind failed" << std::endl;

        return std::optional<std::function<void(void)>>();
    }

    m_NetworkHandlerThread = std::make_unique<std::thread>(&NetworkCommandWatcher::watch, this);

    return [this]() {
        m_ShouldStop = true;

        while (m_ShouldStop) {
            // wait for the thread to stop
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }

        close(m_Socket);
    };
}

void NetworkCommandWatcher::watch() noexcept
{
    std::array<char, MAXLINE> buffer({});

    socklen_t len;

    while (!m_ShouldStop) {
        std::stringstream ss;

        int n = recvfrom(m_Socket, (void*)buffer.data(), buffer.max_size(), MSG_WAITALL, (struct sockaddr*)&servaddr, &len);

        if (n > 0) {
            buffer[std::min<int>(n, buffer.max_size() - 1)] = '\0';

            ss << std::string(buffer.data());

            std::string command;

            ss >> command;

            if (command == "play") {
                enqueueCommand(new PlayDecoderCommand());

                std::cout << "play command sent" << std::endl;
            } else if (command == "loadfile") {
                std::string parameter;

                ss >> parameter;

                enqueueCommand(new LoadFileDecoderCommand(parameter));
            } else if (command == "pause") {
                enqueueCommand(new PauseDecoderCommand());

                std::cout << "Pause command sent" << std::endl;
            } else if (command == "resume") {
                enqueueCommand(new ResumeDecoderCommand());

                std::cout << "Resume command sent" << std::endl;
            } else if (command == "stop") {
                enqueueCommand(new StopDecoderCommand());

                std::cout << "Stop command sent" << std::endl;
            } else if (command == "seek") {
                double pt;

                ss >> pt;

                enqueueCommand(new SeekDecoderCommand(pt));

                std::cout << "Seek at " << pt << "s command sent" << std::endl;
            } else {
                std::cerr << "Unsupported command \"" << command << "\"" << std::endl;
            }
        }
    }

    m_ShouldStop = false;
}