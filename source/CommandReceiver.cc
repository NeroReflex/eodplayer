#include "CommandReceiver.h"

CommandReceiver::CommandReceiver(DequeHM<DecoderCommand*>& commandsQueue) noexcept : m_CommandsQueue(commandsQueue) {}

CommandReceiver::~CommandReceiver() {}

void CommandReceiver::enqueueCommand(DecoderCommand* command) noexcept
{
    m_CommandsQueue.push_back(std::move(command));
}
