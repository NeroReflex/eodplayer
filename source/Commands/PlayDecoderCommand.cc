#include "Commands/PlayDecoderCommand.h"

PlayDecoderCommand::PlayDecoderCommand() noexcept : DecoderCommand() {}

PlayDecoderCommand::~PlayDecoderCommand() {}

void PlayDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->play();
}