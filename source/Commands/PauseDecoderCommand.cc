#include "Commands/PauseDecoderCommand.h"

PauseDecoderCommand::PauseDecoderCommand() noexcept : DecoderCommand() {}

PauseDecoderCommand::~PauseDecoderCommand() {}

void PauseDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->pause();
}