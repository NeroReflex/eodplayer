#include "Commands/StopDecoderCommand.h"

StopDecoderCommand::StopDecoderCommand() noexcept : DecoderCommand() {}

StopDecoderCommand::~StopDecoderCommand() {}

void StopDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->stop();
}