#include "Commands/SeekDecoderCommand.h"

SeekDecoderCommand::SeekDecoderCommand(double time) noexcept : DecoderCommand()
{
    timeToJump = time;
}

SeekDecoderCommand::~SeekDecoderCommand() {}

void SeekDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->seek(timeToJump);
}