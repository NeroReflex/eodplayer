#include "Commands/ResumeDecoderCommand.h"

ResumeDecoderCommand::ResumeDecoderCommand() noexcept : DecoderCommand() {}

ResumeDecoderCommand::~ResumeDecoderCommand() {}

void ResumeDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->resume();
}