#include "Commands/LoadFileDecoderCommand.h"

LoadFileDecoderCommand::LoadFileDecoderCommand(Decoder::FileNameType fileName) noexcept : DecoderCommand(), m_FileName(std::move(fileName)) {}

LoadFileDecoderCommand::~LoadFileDecoderCommand() {}

void LoadFileDecoderCommand::execute(Decoder* target) const noexcept
{
    return target->loadFile(m_FileName);
}