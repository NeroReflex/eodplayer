#include "OpenGLOutputDevice.h"

#include <cassert>

static const std::string vertexShader = "#version 450\n"
                                        "\n"
                                        "const vec2 vQuadPosition[6] = {\n"
                                        "	vec2(-1, -1),\n" // Angolo sinistro
                                        "	vec2(+1, -1),\n" // Angolo destro
                                        "	vec2(-1, +1),\n" // Angolo superiore
                                        "	vec2(-1, +1),\n" // Angolo sinistro
                                        "	vec2(+1, +1),\n" // Angolo destro
                                        "	vec2(+1, -1),\n" // Angolo superiore
                                        "};\n"
                                        "\n"
                                        "layout (location = 0) out vec2 outPos;\n"
                                        "layout (location = 1) out vec2 outUV;\n"
                                        "void main() {\n"
                                        "   outPos = vQuadPosition[gl_VertexID].xy;\n"
                                        "   outUV=vec2((vQuadPosition[gl_VertexID].x + 1.0) / 2.0, 1.0 - "
                                        "((vQuadPosition[gl_VertexID].y + 1.0) / 2.0));\n"
                                        "	gl_Position = vec4(vQuadPosition[gl_VertexID], 0.0, 1.0);\n"
                                        "}\n";

static const std::string fragmentShader = "#version 450"
                                          "\n"
                                          "layout (location = 0) out vec4 presentedColor;"
                                          "layout (location = 0) in vec2 outPos;\n"
                                          "layout (location = 1) in vec2 outUV;\n"
                                          "\n"
                                          "layout (binding = 0) uniform usampler2D frameData;\n"
                                          "\n"
                                          "void main() {\n"
                                          "   vec2 textureUV = (outUV.x <= 0.5) ? vec2(outUV.x * 2.0, outUV.y) : "
                                          "vec2((outUV.x - 0.5) * 2.0, outUV.y);\n"
                                          "   const uvec4 rawPx = texture(frameData, textureUV);\n"
                                          "   const vec4 px = vec4(float(rawPx.r) / 65535.0, float(rawPx.g) / "
                                          "65535.0, float(rawPx.b) / 65535.0, float(rawPx.a) / 65535.0);\n"
                                          "   if (outPos.x >= 0.0) {\n"
                                          "       presentedColor = vec4(px.a, px.a, px.a, 1.0);\n"
                                          "   } else {\n"
                                          "       presentedColor = vec4(px.rgb, 1.0);\n"
                                          "   }\n"
                                          "}\n";

void fill_audio(void* udata, Uint8* stream, int len) noexcept
{
    auto that = reinterpret_cast<OpenGLOutputDevice*>(udata);

    std::lock_guard<std::mutex> audioLck(that->m_AudioMutex);

    int remainingBufferToFillInBytes = len;

    auto sampleSize = 1;
    switch (that->m_WantedAudioSpec.format) {
        case AUDIO_S32:
            sampleSize = 4;
            break;

        default:
            sampleSize = 2;
            break;
    }

    const auto bytesPerSampleAllChannels = sampleSize * that->m_WantedAudioSpec.channels;

    const auto timeOfASample = static_cast<double>(1) / static_cast<double>(that->m_WantedAudioSpec.freq);

    if (that->m_LastFrameRendered.has_value()) {
        const auto& lastFrameRendered         = that->m_LastFrameRendered.value();
        const auto  timestampOfLastShownFrame = lastFrameRendered.timestamp;

        while ((remainingBufferToFillInBytes > 0) && (!that->m_AudioData.empty())) {
            // copy current audio buffer
            auto as = std::move(that->m_AudioData.front());

            // remove current audio buffer from list
            that->m_AudioData.pop_front();

            auto timeDifferenceInSeconds = timestampOfLastShownFrame - as.timestamp;

            const auto bufferSizeInBytes = as.soundBuffer.size();

            const auto availableSamples = bufferSizeInBytes / bytesPerSampleAllChannels;

            const auto duration = (static_cast<double>(availableSamples) * timeOfASample);

            std::vector<uint8_t> currentSample(bytesPerSampleAllChannels);

            // audio/video synchronization at audio packet level
            if (timeDifferenceInSeconds > duration) {
                // if this audio packet is way too old/out-of-sync it's useless. discard it totally
                // std::cout << "Packet discarded: too old" << std::endl;
                continue;
            } else if (as.timestamp + duration < timestampOfLastShownFrame) {
                // if this audio packet is way too early to be reproduced just keep it for later on...
                that->m_AudioData.emplace_front(std::move(as));
                break;
            }

            // foreach sample on every channel....
            size_t consumedSamples;
            for (consumedSamples = 0; (consumedSamples < availableSamples) && (remainingBufferToFillInBytes > 0); ++consumedSamples) {
                // fill current sample with data
                for (size_t j = 0; j < currentSample.size(); ++j) {
                    currentSample[j] = as.soundBuffer[(consumedSamples * bytesPerSampleAllChannels) + j];
                }

                // const auto pushCurrentSampleToBuffer = [&]() {
                if (remainingBufferToFillInBytes < currentSample.size()) {
                    return;
                }

                std::copy(currentSample.cbegin(), currentSample.cend(), stream + (len - remainingBufferToFillInBytes));

                remainingBufferToFillInBytes -= currentSample.size();
                //};

                // TODO: audio/video synchronization at samples level
                // pushCurrentSampleToBuffer();
            }

            // if there are samples not played it is important to put them back in the audio buffer
            if (consumedSamples < availableSamples) {
                const auto lostTimeInSeconds = static_cast<double>(consumedSamples) * timeOfASample;

                /*
                std::vector<uint8_t> remainingBytes((availableSamples - consumedSamples) * currentSample.size());
                std::copy(as.soundBuffer.cbegin() + (consumedSamples * currentSample.size()), as.soundBuffer.cend(), remainingBytes.begin());
                as.soundBuffer = std::move(remainingBytes);
                */

                as.timestamp += lostTimeInSeconds;
                as.soundBuffer.erase(as.soundBuffer.begin(), as.soundBuffer.begin() + (consumedSamples * currentSample.size()));

                that->m_AudioData.emplace_front(std::move(as));
            }
        }
    }

    // at the end make sure the buffer has been filled by putting silence at the end if not enough samples were added
    if (remainingBufferToFillInBytes > 0) {
        std::vector<uint8_t> silence(remainingBufferToFillInBytes);
        std::fill(silence.begin(), silence.end(), 0);

        // SDL_MixAudio(stream + (len - remainingBufferToFillInBytes), silence.data(), remainingBufferToFillInBytes, SDL_MIX_MAXVOLUME);
        std::copy(silence.cbegin(), silence.cbegin() + remainingBufferToFillInBytes, stream + (len - remainingBufferToFillInBytes));

        remainingBufferToFillInBytes -= remainingBufferToFillInBytes;
    }
}

OpenGLOutputDevice::OpenGLOutputDevice(FrameStreamHandler& queueHandler, int window_width, int window_height) noexcept :
    OutputDevice(queueHandler), m_WindowWidth(window_width), m_WindowHeight(window_height), m_Window(nullptr), m_ContextInitialized(false), m_Running(true),
    m_OpenGLRenderingData()
{
    // Hint to OpenGL 4.5 context
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

    // Turn on double buffering with a 16 bit Z buffer (the Z buffer is at
    // minimum as it's not even used).
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    m_WantedAudioSpec.freq     = 44100;
    m_WantedAudioSpec.format   = AUDIO_S32;
    m_WantedAudioSpec.channels = 1;
    m_WantedAudioSpec.samples  = 1024;
    m_WantedAudioSpec.callback = fill_audio;
    m_WantedAudioSpec.userdata = reinterpret_cast<void*>(this);

    // m_SilenceAudioSamples =

    m_Window = SDL_CreateWindow(
        "EODPlayer", 0, 0, m_WindowWidth, m_WindowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS /* | SDL_WINDOW_ALWAYS_ON_TOP */
    );
}

OpenGLOutputDevice::~OpenGLOutputDevice()
{
    if (m_ContextInitialized) {
        // Destroy window and audio device if it has been created
        if (m_Window != nullptr) {
            SDL_CloseAudio();
            SDL_DestroyWindow(m_Window);
        }

        // delete the OpenGL context
        SDL_GL_DeleteContext(m_Context);

        // here execute the program cleanup
        auto it = m_RenderingPrograms.begin();
        while (it != m_RenderingPrograms.end()) {
            glDeleteProgram(it->second);
            it = m_RenderingPrograms.erase(it);
        }
    }
}

int OpenGLOutputDevice::compileProgram(Frame::PixelFormat pf, const std::string& vs, const std::string& fs) noexcept
{
    const GLchar* const vsSrc[] = {vs.c_str()};
    const GLchar* const fsSrc[] = {fs.c_str()};

    GLint isCompiled;

    // create, compile and check for vertex shader compilation result
    GLuint vertexShaderObj = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShaderObj, 1, vsSrc, nullptr);
    glCompileShader(vertexShaderObj);
    glGetShaderiv(vertexShaderObj, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE) {
        GLint maxLength = 0;
        glGetShaderiv(vertexShaderObj, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar* errorLog = new GLchar[maxLength];
        glGetShaderInfoLog(vertexShaderObj, maxLength, &maxLength, &errorLog[0]);

        std::cerr << "In GL_VERTEX_SHADER: " << errorLog << std::endl;

        delete[] errorLog;

        // Exit with failure.
        glDeleteShader(vertexShaderObj); // Don't leak the shader.
        return -1;
    }

    // same thing for fragment shader
    GLuint fragmentShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShaderObj, 1, fsSrc, nullptr);
    glCompileShader(fragmentShaderObj);
    glGetShaderiv(fragmentShaderObj, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE) {
        GLint maxLength = 0;
        glGetShaderiv(fragmentShaderObj, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar* errorLog = new GLchar[maxLength];
        glGetShaderInfoLog(fragmentShaderObj, maxLength, &maxLength, &errorLog[0]);

        std::cerr << "In GL_FRAGMENT_SHADER: " << errorLog << std::endl;

        delete[] errorLog;

        // Exit with failure...

        // Don't leak the previously compiled shader
        glDeleteShader(vertexShaderObj);

        // Don't leak the shader.
        glDeleteShader(fragmentShaderObj);

        return -2;
    }

    // create the program, link it and check result
    GLuint renderingProgramObj = glCreateProgram();
    glAttachShader(renderingProgramObj, vertexShaderObj);
    glAttachShader(renderingProgramObj, fragmentShaderObj);
    glLinkProgram(renderingProgramObj);

    GLint isLinked = 0;
    glGetProgramiv(renderingProgramObj, GL_LINK_STATUS, (int*)&isLinked);
    if (isLinked == GL_FALSE) {
        GLint maxLength = 0;
        glGetProgramiv(renderingProgramObj, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar* infoLog = new GLchar[maxLength];
        glGetProgramInfoLog(renderingProgramObj, maxLength, &maxLength, &infoLog[0]);

        std::cerr << "In linking: " << infoLog << std::endl;

        delete[] infoLog;

        // We don't need the program anymore.
        glDeleteProgram(renderingProgramObj);

        // Don't leak shaders either.
        glDeleteShader(vertexShaderObj);
        glDeleteShader(fragmentShaderObj);

        return -3;
    }

    glDetachShader(renderingProgramObj, vertexShaderObj);
    glDetachShader(renderingProgramObj, fragmentShaderObj);

    // associate the current program with its format
    m_RenderingPrograms[pf] = renderingProgramObj;

    // shader cleanup
    glDeleteShader(vertexShaderObj);
    glDeleteShader(fragmentShaderObj);

    // activate the program, this is a lengthy operation to perform and doing it
    // only once is ideal
    glUseProgram(renderingProgramObj);
    m_ActiveProgram = pf;

    return 0;
}

void OpenGLOutputDevice::exec() noexcept
{
    if (SDL_OpenAudio(&m_WantedAudioSpec, nullptr) < 0) {
        std::cerr << "Cannot open audio device" << std::endl;

        return;
    }

    if (!m_ContextInitialized) {
        if (!m_Window) {
            std::cerr << "Error: cannot open window" << std::endl;
            return;
        }

        // Disable cursor inside the window
        SDL_ShowCursor(SDL_DISABLE);

        // create OpenGL context
        m_Context = SDL_GL_CreateContext(m_Window);
        if (!m_Context) {
            std::cerr << "Error on SDL_GL_CreateContext: " << SDL_GetError() << std::endl;
            return;
        }

        glewExperimental = true;
        GLenum err       = glewInit();
        if (err != GLEW_OK) {
            SDL_GL_DeleteContext(m_Context);

            std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
            return;
        }

        if (!GLEW_VERSION_4_5) {
            SDL_GL_DeleteContext(m_Context);

            std::cerr << "Error: OpenGL 4.5 is not supported" << std::endl;
            return;
        }

        // VSync: This makes our buffer swap syncronized with the monitor's
        // vertical refresh (to avoid tearing)
        SDL_GL_SetSwapInterval(1);

        if (compileProgram(Frame::PixelFormat::RGBA64, vertexShader, fragmentShader) != 0) {
            SDL_GL_DeleteContext(m_Context);

            std::cerr << "Cannot compile OpenGL program" << std::endl;
        }

        // clip window
        glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);

        // setup viewport 0
        glViewportIndexedf(0, 0, 0, (float)m_WindowWidth, (float)m_WindowHeight);

        // load the black frame
        glBindVertexArray(m_EmptyFrameVAO);
        glEnable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &m_EmptyImageTexture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        std::vector<uint16_t> emptyFrameData(m_WindowWidth * m_WindowHeight * 4);
        std::fill(emptyFrameData.begin(), emptyFrameData.end(), 0);

        // load frame image into a texture
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16UI, m_WindowWidth, m_WindowHeight);
        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGBA16UI, m_WindowWidth, m_WindowHeight, 0, GL_RGBA_INTEGER, GL_UNSIGNED_SHORT, reinterpret_cast<void*>(emptyFrameData.data())
        );
        glBindTextures(0, 1, &m_EmptyFrameVAO);

        // flag the context as initialized
        m_ContextInitialized = true;
    }

    // glViewport(0, 0, window_width, window_height);

    while (m_Running) {
        const auto cycleBeginTime = std::chrono::high_resolution_clock::now();

        auto firstFrameIt = m_OpenGLRenderingData.begin();

        std::optional<double> timeDifferenceInSecondsBetweenLastFrameRendertimeAndNow;
        std::optional<double> timeDifferenceInSecondsBetweenLastFrameAndNextToRenderTimestamps;

        const bool nextFrameReady        = firstFrameIt != m_OpenGLRenderingData.end();
        const bool previousFrameRendered = m_LastFrameRendered.has_value();

        if (previousFrameRendered) {
            timeDifferenceInSecondsBetweenLastFrameRendertimeAndNow =
                static_cast<double>(
                    std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - m_LastFrameRendered.value().rendertime)
                        .count()
                ) /
                static_cast<double>(1000000000.0);

            if (nextFrameReady) {
                timeDifferenceInSecondsBetweenLastFrameAndNextToRenderTimestamps = firstFrameIt->timestamp - m_LastFrameRendered.value().timestamp;
            }
        }

        const auto   a              = timeDifferenceInSecondsBetweenLastFrameRendertimeAndNow.value_or(-1.0);
        const auto   b              = timeDifferenceInSecondsBetweenLastFrameAndNextToRenderTimestamps.value_or(1.0);
        const double timeDifference = (a - b);

        const bool timeToShowNextFrame = (nextFrameReady) && ((!previousFrameRendered) || (timeDifference >= 0.0));

        auto processVideoData = [this](const std::optional<VideoData>& arrived) -> bool {
            if (!arrived.has_value()) {
                return false;
            }

            const auto& vd = arrived.value();

            // this is safe as the frame currently rendering is
            // the next one
            PrepareFrameForRendering(arrived.value());

            return true;
        };

        switch (pollDecoderStatus()) {
            case DecoderStatus::Active:
                {
                    if (timeToShowNextFrame) {
                        RenderFrame(firstFrameIt);
                    } else if (m_OpenGLRenderingData.size() < MaxReadyFrames) {
                        processVideoData(receiveVideoData());
                    }
                }
                break;

            case DecoderStatus::Paused:
                {
                    // Pause audio
                    SDL_PauseAudio(1);

                    // at some point the decoder will exit from pause, so
                    // transfer frames on-the-fly to this OutputDevice so that
                    // at resume it won't find the queue full. ! doing this MUST
                    // NOT influence the playback visible to the user in any
                    // way, it's something done to improve the application
                    // memory management
                    if (m_OpenGLRenderingData.size() < MaxReadyFrames) {
                        processVideoData(receiveVideoData());
                    } else {
                        // the local queue of frames readies to be played is full,
                        // therefore a thread sleep could be issued. Let's say
                        // that the resume command will come from the user
                        // pressing a button... if the whole resume operation
                        // will take more than 4ms the delay will feel strange
                        // to the user, therefore a sleep of less MUST be
                        // issued.

                        // To be on the safe side just wait 1 millisecond as the
                        // provided time is the MINIMUM time for the sleep,
                        // worst case scenario the delay will be of 4ms.
                        std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    }
                }
                break;

            case DecoderStatus::Seeking:
                {
                    // Pause audio
                    SDL_PauseAudio(1);

                    if (nextStream()) {
                        m_LastFrameRendered.reset();

                        // flag all frame opengl structures to be
                        // recycled
                        auto it = m_OpenGLRenderingData.begin();
                        while (it != m_OpenGLRenderingData.end()) {
                            m_RecycledOpenGLRenderingData.push_back(*it);
                            it = m_OpenGLRenderingData.erase(it);
                        }

                        // remove all old audio samples
                        m_AudioData.clear();
                    }
                }
                break;

            case DecoderStatus::Unknown:
            case DecoderStatus::WaitingForFile:
            case DecoderStatus::WaitingForPlay:
                {
                    // Pause audio
                    SDL_PauseAudio(1);

                    // render the black frame
                    RenderBlackFrame();
                }
                break;
            case DecoderStatus::Stopped:
                {
                    if (timeToShowNextFrame) {
                        RenderFrame(firstFrameIt);
                    } else if (m_OpenGLRenderingData.size() < MaxReadyFrames) {
                        auto possibly_frame = receiveVideoData();
                        if (processVideoData(possibly_frame)) {
                            // the data has been prepared for reproduction
                        } else if ((m_OpenGLRenderingData.empty())) {
                            // there is no frame on the current queue, the
                            // decoder is in stopped status so either a decoding
                            // operation has ended.

                            // render the black frame
                            RenderBlackFrame();

                            // so this might be the best moment to change stream
                            // if another one is available
                            if (nextStream()) {
                                SDL_PauseAudio(1);

                                m_LastFrameRendered.reset();

                                // flag all frame opengl structures to be
                                // recycled
                                auto it = m_OpenGLRenderingData.begin();
                                while (it != m_OpenGLRenderingData.end()) {
                                    m_RecycledOpenGLRenderingData.push_back(*it);
                                    it = m_OpenGLRenderingData.erase(it);
                                }

                                // remove all old audio samples
                                m_AudioData.clear();
                            }
                        }
                    }
                }
        }

        // const auto cycleMiddleTime =
        // std::chrono::high_resolution_clock::now();

        // this must be done in every case otherwise some graphical providers
        // will tell the user the application is not responding
        SDL_Event ev;
        int       i = 0;
        while (SDL_PollEvent(&ev)) {
            if (ev.key.keysym.sym == SDLK_ESCAPE)
                m_Running = false; // Exit when esc
        }
        // const auto cycleEndTime = std::chrono::high_resolution_clock::now();

        const auto cycleEndTime = std::chrono::high_resolution_clock::now();

        /*std::cout << "Spent " << std::chrono::duration<double,
           std::milli>(cycleEndTime - cycleBeginTime).count()
                  << "ms to do " << action << ", where events handling required
           "
                  << std::chrono::duration<double, std::milli>(cycleEndTime -
           cycleMiddleTime).count()
                  << "ms while I had " << m_OpenGLRenderingData.size() << "
           frames ready\n";*/
    }

    // terminate audio output
    SDL_PauseAudio(1);

    // as the resources needed for rendering are created at the beginning of
    // exec let's destroy them so that a future exec call can recreate them
    // without leaking memory
    for (auto it = m_OpenGLRenderingData.begin(); it != m_OpenGLRenderingData.end(); ++it) {
        glDeleteVertexArrays(1, &it->frameVAO);

        glDeleteTextures(1, &it->imageTexture);
    }

    std::cout << std::endl;

    m_OpenGLRenderingData.clear();
}

void OpenGLOutputDevice::RenderFrame(std::list<FrameOpenGLStructure>::iterator it) noexcept
{
    // clear color buffer of current framebuffer(0)
    glClearNamedFramebufferfv(0, GL_COLOR, 0, ClearColor);

    // change program only when it's necessary to avoid slowdowns
    if (m_ActiveProgram != it->pf) {
        glUseProgram(m_RenderingPrograms[it->pf]);

        m_ActiveProgram = it->pf;
    }

    // bind vertex array object containing the frame
    glBindVertexArray(it->frameVAO);

    // glActiveTexture(GL_TEXTURE0); // activate the texture unit first before
    // binding texture glBindTexture(GL_TEXTURE_2D, it->imageTexture);

    // here 6 because of how the vertex shader is written...
    glDrawArrays(GL_TRIANGLES, 0, 6);

    // swap buffers presenting the next frame (if enough time has passed)
    SDL_GL_SwapWindow(m_Window);

    /*
if ((it->hasSound) && (!it->soundBuffer.empty())) {
    int sound_playback_error;
    pa_simple_write(m_PulseaudioSimple, reinterpret_cast<const void*>(it->soundBuffer.data()), it->soundBuffer.size(), &sound_playback_error);
}
    */

    FrameRenderedInfo lastRendered{.rendertime = std::chrono::high_resolution_clock::now(), .timestamp = it->timestamp};

    if (m_LastFrameRendered.has_value()) {
        SDL_PauseAudio(0);
    }

    m_LastFrameRendered = lastRendered;

    // schedule node for recycle
    m_RecycledOpenGLRenderingData.push_back(*it);

    // erase from the active list
    m_OpenGLRenderingData.erase(it);
}

void OpenGLOutputDevice::PrepareFrameForRendering(const VideoData& vd) noexcept
{
    const int64_t timebase_num = vd.getTimeBaseNum();
    const int64_t timebase_den = vd.getTimeBaseDen();
    const int64_t timestamp    = vd.getTimeTicks();

    const double composedTimestamp = (static_cast<double>(timebase_num) * static_cast<double>(timestamp)) / static_cast<double>(timebase_den);

    if (vd.hasFrame()) {
        const auto& frame = vd.getFrame();

        FrameOpenGLStructure renderFrameCtx = {};

        if (m_RecycledOpenGLRenderingData.empty()) {
            glGenVertexArrays(1, &renderFrameCtx.frameVAO);
        } else {
            auto elementToRecycle = m_RecycledOpenGLRenderingData.begin();
            renderFrameCtx        = *(elementToRecycle);
            m_RecycledOpenGLRenderingData.erase(elementToRecycle);
        }

        if (frame.isHoldingData()) {

            GLenum format;
            GLenum type;
            GLenum internalFormat;

            switch (frame.getPixelFormat()) {
                case Frame::PixelFormat::RGBA64:
                    format         = GL_RGBA_INTEGER;
                    type           = GL_UNSIGNED_SHORT;
                    internalFormat = GL_RGBA16UI;
                    break;
            }

            glBindVertexArray(renderFrameCtx.frameVAO);

            glEnable(GL_TEXTURE_2D);

            glActiveTexture(GL_TEXTURE0);

            if (m_RecycledOpenGLRenderingData.empty()) {
                glGenTextures(1, &renderFrameCtx.imageTexture);
                // glBindTexture(GL_TEXTURE_2D, renderFrameCtx.frameVAO);
            }

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

            const auto image_width  = frame.getWidth();
            const auto image_height = frame.getHeight();

            const auto buffer = frame.getRawBuffer();

            // load frame image into a texture
            glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, image_width, image_height);
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, image_width, image_height, 0, format, type, buffer);

            glBindTextures(0, 1, &renderFrameCtx.frameVAO);

            renderFrameCtx.pf = frame.getPixelFormat();

            // write timestamp
            renderFrameCtx.timestamp = composedTimestamp;

            m_OpenGLRenderingData.push_back(renderFrameCtx);

            // now, it is possible that some frames are out-of-order as they are decoded
            // after a frame that has to be shown before, for this reason it's
            // imperative to sort the elements by timestamp
            m_OpenGLRenderingData.sort([](const FrameOpenGLStructure& a, const FrameOpenGLStructure& b) -> bool { return a.timestamp < b.timestamp; });
        }
    }

    if (vd.hasAudio()) {
        const auto& audio = vd.getAudioBuffer();

        AudioStructure as;

        if (audio.isHoldingData()) {
            as.soundBuffer = std::vector<uint8_t>(
                reinterpret_cast<const uint8_t*>(audio.getRawBuffer()), reinterpret_cast<const uint8_t*>(audio.getRawBuffer()) + audio.getRawBufferSize()
            );

            as.timestamp = composedTimestamp;

            // lock the audio data mutex
            std::lock_guard<std::mutex> audioLck(m_AudioMutex);

            m_AudioData.emplace_back(std::move(as));

            // make sure audio data is ordered by timestamp
            m_AudioData.sort([](const AudioStructure& a, const AudioStructure& b) -> bool { return a.timestamp < b.timestamp; });
        }
    }
}

void OpenGLOutputDevice::RenderBlackFrame() noexcept
{
    // clear color buffer of current framebuffer(0)
    glClearNamedFramebufferfv(0, GL_COLOR, 0, ClearColor);

    // the empty frame is in RGBA64 format
    if (m_ActiveProgram != Frame::PixelFormat::RGBA64) {
        glUseProgram(m_RenderingPrograms[Frame::PixelFormat::RGBA64]);
    }

    // bind vertex array object containing the empty frame
    glBindVertexArray(m_EmptyFrameVAO);

    // glActiveTexture(GL_TEXTURE0); // activate the texture unit first before
    // binding texture glBindTexture(GL_TEXTURE_2D, it->imageTexture);

    // here 6 because of how the vertex shader is written...
    glDrawArrays(GL_TRIANGLES, 0, 6);

    // swap buffers presenting the next frame (if enough time has passed)
    SDL_GL_SwapWindow(m_Window);
}